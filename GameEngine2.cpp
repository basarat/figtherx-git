#include "FIGHTERX.h"
#include "CFighter.h"

extern unsigned short int max_frame_rate;
extern unsigned short int time_slice;

extern CDXScreen* Screen;

extern CFighter*g_FirstFighter;
extern CFighter*g_FirstFighter1;
extern CFighter*g_FirstFighter2;
extern CFighter*g_SecondFighter;
extern CFighter*g_SecondFighter1;
extern CFighter*g_SecondFighter2;


extern CDXSprite* g_BruceSplash;
extern CDXSprite* g_CyrusSplash;
extern CDXSprite* g_LadySplash;
extern CDXSprite* g_JuniorSplash;

void SetFrameRate(int x)
{
     max_frame_rate = x;
     time_slice = (1000/max_frame_rate);
}


void cdx_UnloadPlayers()
{
    
    delete g_FirstFighter1->GetFire();
    SAFEDELETE(g_FirstFighter1);
    delete g_FirstFighter2->GetFire();
    SAFEDELETE(g_FirstFighter2);
    
    delete g_SecondFighter1->GetFire();
    SAFEDELETE(g_SecondFighter1);
    delete g_SecondFighter2->GetFire();
    SAFEDELETE(g_SecondFighter2);
    
       

}


void cdx_LoadSplashScreens()
{
  g_LadySplash=new CDXSprite;

  g_LadySplash->Create(Screen, "./Gfx/BLSPLASH.bmp", 485, 246 , 1 , CDXMEM_SYSTEMONLY);
    
  g_LadySplash->SetColorKey();


  g_BruceSplash=new CDXSprite;
  g_BruceSplash->Create(Screen, "./Gfx/BASSPLASH.bmp", 485, 246 , 1 , CDXMEM_SYSTEMONLY);
  g_BruceSplash->SetColorKey();

  g_CyrusSplash=new CDXSprite;
  g_CyrusSplash->Create(Screen, "./Gfx/CYRUSSPLASH.bmp", 485, 246 , 1 , CDXMEM_SYSTEMONLY);
  g_CyrusSplash->SetColorKey();
    

  
  g_JuniorSplash=new CDXSprite;
  g_JuniorSplash->Create(Screen, "./Gfx/JUNIORSPLASH.bmp", 485, 246 , 1 , CDXMEM_SYSTEMONLY);
  g_JuniorSplash->SetColorKey();
    

    

}



































/*
void cdx_ShowTiles()
{
     double MapX
        ,MapY
        , TileType;

    Screen->GetBack()->GetDC( );

    char Text[256];
    	
	  
    
    
	
    

    
    // Check top left
    MapX = (Map->GetPosX() + g_FirstFighter->GetPosX()+g_FirstFighter->m_rcCollision.left )*1.0 / Map->GetTileWidth();
    MapY = (Map->GetPosY() + g_FirstFighter->GetPosY()+g_FirstFighter->m_rcCollision.top )*1.0 / Map->GetTileHeight();
    TileType = Map->GetTile(min(MapX, Map->GetMapWidth()-1), min(MapY, Map->GetMapHeight()-1));
    
    sprintf( Text , "Sprite Top Left:%2.0f",TileType );
    		
	  Screen->GetBack()->TextXY( 10 , 100 , RGB( 0 , 0 , 0 ) , Text );
    



   // else
   // {
        //TileType = Map->GetTile(min(MapX,Map->GetMapWidth()-1), min(MapY+.5, Map->GetMapHeight()-1));
        //if(TileType == Tile) return TRUE;
    //}


    // Check top right
    MapX = (Map->GetPosX() + g_FirstFighter->GetPosX() + g_FirstFighter->GetTile()->m_BlockWidth-g_FirstFighter->m_rcCollision.right)*1.0 / Map->GetTileWidth();
    MapY = (Map->GetPosY() + g_FirstFighter->GetPosY()+ g_FirstFighter->m_rcCollision.top )*1.0 / Map->GetTileHeight();
    TileType = Map->GetTile(min(MapX, Map->GetMapWidth()-1), min(MapY, Map->GetMapHeight()-1));
   
    
    sprintf( Text , "Sprite Top Right: %2.0f",TileType);
    
    Screen->GetBack()->TextXY( 10 , 120 , RGB( 0 , 0 , 0 ) , Text );



    //else
    //{
     //  TileType = Map->GetTile(min(MapX, Map->GetMapWidth()-1), min(MapY-1.5, Map->GetMapHeight()-1));
     //  if(TileType == Tile) return TRUE;
    //}


    // Check bottom left
    MapX = (Map->GetPosX() + g_FirstFighter->GetPosX()+g_FirstFighter->m_rcCollision.left )*1.0/ Map->GetTileWidth();
    MapY = (Map->GetPosY() + g_FirstFighter->GetPosY() + g_FirstFighter->GetTile()->m_BlockHeight-g_FirstFighter->m_rcCollision.bottom)*1.0 / Map->GetTileHeight();
    TileType = Map->GetTile(min(MapX, Map->GetMapWidth()-1), min(MapY, Map->GetMapHeight()-1));
   
    sprintf(Text ,"Sprite Bottom Left: %2.0f ",TileType );
    
    Screen->GetBack()->TextXY( 10 , 140 , RGB( 0 , 0 , 0 ) , Text );

   
    {
       TileType = Map->GetTile(min(MapX, Map->GetMapWidth()-1), min(MapY-1,Map->GetMapHeight()-1));
     
    }
    


    // Check bottom right
    MapX = (Map->GetPosX() + g_FirstFighter->GetPosX() + g_FirstFighter->GetTile()->m_BlockWidth-g_FirstFighter->m_rcCollision.right) *1.0/ Map->GetTileWidth();
    MapY = (Map->GetPosY() + g_FirstFighter->GetPosY() + g_FirstFighter->GetTile()->m_BlockHeight-g_FirstFighter->m_rcCollision.bottom) *1.0/ Map->GetTileHeight();
    TileType = Map->GetTile(min(MapX, Map->GetMapWidth()-1), min(MapY, Map->GetMapHeight()-1));
 
    sprintf(Text ,"Sprite Bottom Right: %2.0f ",TileType );
    
    Screen->GetBack()->TextXY( 10 , 160 , RGB( 0 , 0 , 0 ) , Text );

 
    {
        TileType = Map->GetTile(min(MapX, Map->GetMapWidth()-1), min(MapY-1,Map->GetMapHeight()-1));
 
    }

    Screen->GetBack()->ReleaseDC( );
}

*/
