//-----------------------------------------------------------------
// Background Object
// C++ Header - Background.h
//-----------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------
#include <windows.h>
#define CDXINCLUDEALL     // this define includes all headers, otherwise include one by one
#include <cdx.h>



//-----------------------------------------------------------------
// Starry Background Class
//-----------------------------------------------------------------
class TileAnimator
{
protected:
  // Member Variables
  CDXMap *  m_Map;
  int       m_iNumTiles;
  int       m_iFrameDelay;
  POINT     m_ptTile;
  
  int       m_TileValues[50];
  int       m_iCurrentTile;

public:
  // Constructor(s)/Destructor
  TileAnimator(CDXMap * Map, int iNumTiles, 
        int UpdateDelay = 0);
  

  virtual ~TileAnimator();

  void SetTiles(int a[],POINT point);
  void SetMap(CDXMap*);
  
  // General Methods
   void  Update();
   

};
