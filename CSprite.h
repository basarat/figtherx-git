// CSprite.h: interface for the CSprite class.
//
//////////////////////////////////////////////////////////////////////

//#if !defined(AFX_CDXDSPRITE_H__73F8ED27_CA9F_4E37_B240_CC1E4C57391A__INCLUDED_)
//#define AFX_CDXDSPRITE_H__73F8ED27_CA9F_4E37_B240_CC1E4C57391A__INCLUDED_

//#if _MSC_VER > 1000
#pragma once
//#endif // _MSC_VER > 1000

#include <windows.h>
#define CDXINCLUDEALL     // this define includes all headers, otherwise include one by one
#include <cdx.h>

typedef DWORD BOUNDSACTION;

const BOUNDSACTION BA_WRAP=0,
                   BA_HIDE=1;              


class CSprite :public CDXSprite
{
public:
	CSprite();
	
  virtual ~CSprite();

  BOOL IsHidden(){return m_bHidden;}

  void SetHidden(BOOL Hidden){m_bHidden=Hidden;m_iCurFrame=m_iStartFrame;}
  
  void SetOneCycle(BOOL Cycle){m_bOneCycle=Cycle;}

  void SetNumFrames(int num){m_iFrames=num;}

  void SetStartFrame(int num);

  void SetCurFrame(int num);

  void Update();

  void Reverse();

  void SetBoundRect(RECT,BOUNDSACTION=BA_WRAP);

  void SetDelay(int i);

  int GetNumFrames(){return m_iFrames;}



private:
    
    
    BOOL m_bHidden;
    BOOL m_bOneCycle;
    int m_iFrames;
    int m_iCurFrame;
    int m_iStartFrame;

    int m_iFrameDelay;
    int m_iFrameTrigger;

    RECT m_BoundRect;
    BOUNDSACTION m_BoundsAction;
    BOOL m_Bounded;
  
    

};

//#endif // !defined(AFX_CDXDSPRITE_H__73F8ED27_CA9F_4E37_B240_CC1E4C57391A__INCLUDED_)
