#include "FighterX.h"
#include "CDXMenu.h"
#include "CDXHorizTextScroller.h"
#include "CDXVertTextScroller.h"


extern CDXBitmapFont* BigFont;
extern CDXBitmapFont* BiggerFont;


extern CDXBitmapFont * CreditsFont;

//RECT PlayerClipRect = { 0 , 0 , 500 , 768 };    


CDXMenu* PlayerOneMenu ;
CDXMenu* PlayerTwoMenu ;

extern CDXSprite * MainMenuLogo;
CDXSprite * PlayerMenuLogo;

#define MENU_ITEM_HEIGHT 40


extern CDXScreen* Screen;
extern CDXInput* Input;


extern int g_ProgramState;
extern HWND g_hWnd;

extern int g_Winner;



extern CDXSprite* g_BruceSplash;
extern CDXSprite* g_CyrusSplash;
extern CDXSprite* g_LadySplash;
extern CDXSprite* g_JuniorSplash;


void cdx_InitPlayerMenu()
{
    PlayerOneMenu=new CDXMenu();
    PlayerOneMenu->Create(Screen->GetBack(), MENU_ITEM_HEIGHT);
    //PlayerOneMenu->SetTitle("Main Menu", RGB(255, 255, 0));

    Screen->GetBack()->ChangeFont("Verdana", 0, MENU_ITEM_HEIGHT, FW_BOLD);


	  // Add some options to the menu
    PlayerOneMenu->AddItem("Bruce", RGB(0, 0, 255), RGB(255,255, 255));
	  PlayerOneMenu->AddItem("Cyrus", RGB(0, 0, 255), RGB(255,255, 255));
	  PlayerOneMenu->AddItem("Lady", RGB(0, 0, 255), RGB(255,255, 255));
    PlayerOneMenu->AddItem("Junior", RGB(0, 0, 255), RGB(255,255, 255));

    PlayerOneMenu->Home();

    PlayerMenuLogo=new CDXSprite;
    PlayerMenuLogo->Create(Screen, "./Gfx/PlayerMenu.jpg", 1024, 768 , 1 , CDXMEM_SYSTEMONLY);
    //Screen->GetBack()->Fill(RGB(255,255,255));
    PlayerMenuLogo->SetPos(0,0);

    

}

void cdx_DoPlayerMenu()
{
    //Input->Update();


    cdx_HandlePlayerMenuKeys();
    
    Screen->GetBack()->ChangeFont("Verdana", 0, MENU_ITEM_HEIGHT, FW_BOLD);
    Screen->GetBack()->SetFont();
    PlayerMenuLogo->Draw(Screen->GetBack( ), 0,	0, CDXBLT_BLK);


    PlayerOneMenu->Draw(100,300);


    switch(PlayerOneMenu->Enter())
    {
    case 0:
        g_BruceSplash->SetPos(500,261);
        g_BruceSplash->Draw(Screen->GetBack() , 0 , 0 , CDXBLT_TRANS);
        break;
    case 1:
        g_CyrusSplash->SetPos(500,261);
        g_CyrusSplash->Draw(Screen->GetBack() , 0 , 0 , CDXBLT_TRANS);
        break;
    case 2:
        g_LadySplash->SetPos(500,261);
        g_LadySplash->Draw(Screen->GetBack() , 0 , 0 , CDXBLT_TRANS);
        break;
    case 3:
        g_JuniorSplash->SetPos(500,261);
        g_JuniorSplash->Draw(Screen->GetBack() , 0 , 0 , CDXBLT_TRANS);
        break;
    }
    

    if(g_ProgramState==PS_PLAYERONEMENU)
    {
        BigFont->DrawTrans(50,100,"PLAYER ONE",Screen->GetBack());
    }
    else
    {
        BigFont->DrawTrans(50,100,"PLAYER TWO",Screen->GetBack());
    }

        


}


void cdx_HandlePlayerMenuKeys()
{
    //MainMenu->Home();
    if(Input->GetKeyState(CDXKEY_UPARROW)==CDXKEY_PRESS)
    {
        PlayerOneMenu->Up();
    }
    
    if(Input->GetKeyState(CDXKEY_DOWNARROW)==CDXKEY_PRESS)
    {
        PlayerOneMenu->Down();
    }

    if(Input->GetKeyState(CDXKEY_ENTER)==CDXKEY_PRESS)
    {
        
        BiggerFont->DrawTrans(550,300,"Loading",Screen->GetFront());
        //Screen->Flip();
        
        switch(PlayerOneMenu->Enter())
        {
            
        

        case 0:
            if(g_ProgramState==PS_PLAYERONEMENU)
            {
                LoadBruce(1);
                g_ProgramState=PS_PLAYERTWOMENU;
            }
            else
            {
                LoadBruce(2);
                cdx_RestartGame();
                g_ProgramState=PS_GAME;
            }

                
            break;
        case 1:
            if(g_ProgramState==PS_PLAYERONEMENU)
            {
                LoadCyrus(1);
                g_ProgramState=PS_PLAYERTWOMENU;
            }
            else
            {
                LoadCyrus(2);
                cdx_RestartGame();
                g_ProgramState=PS_GAME;
            }

            break;
        case 2:
            if(g_ProgramState==PS_PLAYERONEMENU)
            {
                LoadLady(1);
                g_ProgramState=PS_PLAYERTWOMENU;
            }
            else
            {
                LoadLady(2);
                cdx_RestartGame();
                g_ProgramState=PS_GAME;
            }

 
            break;
        case 3:
            if(g_ProgramState==PS_PLAYERONEMENU)
            {
                LoadJunior(1);
                g_ProgramState=PS_PLAYERTWOMENU;
            }
            else
            {
                LoadJunior(2);
                cdx_RestartGame();
                g_ProgramState=PS_GAME;
            }

 
            break;

        

        }
        
    }


}

void cdx_DeInitPlayerMenu()
{
  // SAFEDELETE(BigFont);
    SAFEDELETE(PlayerOneMenu);

}



