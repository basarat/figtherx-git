#include "FighterX.h"
#include "CDXMenu.h"
#include "CDXHorizTextScroller.h"
#include "CDXVertTextScroller.h"
#include "TileAnimator.h"


extern CDXBitmapFont* BigFont;


extern CDXBitmapFont * CreditsFont;
//RECT CreditsClipRect = { 0 , 0 , 500 , 768 };    




extern CDXSprite *PlayerMenuLogo;
extern CDXSprite * MainMenuLogo;

#define MENU_ITEM_HEIGHT 40


extern CDXScreen* Screen;
extern CDXInput* Input;


extern int g_ProgramState;
extern HWND g_hWnd;

extern int g_Winner;


extern CDXMap * Map;
extern CDXTile * Tiles;

extern TileAnimator * g_TileAnimator;
extern TileAnimator * g_TileAnimator2;
    

CDXMenu* MapMenu;


CDXMap* Map1;
CDXMap* Map2;
CDXMap* Map3;

void cdx_InitMapMenu()
{
    Map1 = new CDXMap(Tiles, Screen);
	  if( Map1->Load("./GFX/MAP1.MAP") == FALSE )
        CDXError( Screen , "Could not load map MAP1.MAP" );
    Map2 = new CDXMap(Tiles, Screen);
	  if( Map2->Load("./GFX/MAP2.MAP") == FALSE )
        CDXError( Screen , "Could not load map MAP2.MAP" );
    Map3 = new CDXMap(Tiles, Screen);
	  if( Map3->Load("./GFX/MAP3.MAP") == FALSE )
        CDXError( Screen , "Could not load map MAP3.MAP" );

    MapMenu=new CDXMenu();
    MapMenu->Create(Screen->GetBack(), MENU_ITEM_HEIGHT);
    //MapMenu->SetTitle("Main Menu", RGB(255, 255, 0));

    Screen->GetBack()->ChangeFont("Verdana", 0, MENU_ITEM_HEIGHT, FW_BOLD);


	  // Add some options to the menu
    MapMenu->AddItem("All Rounder", RGB(255, 0, 0), RGB(255,255, 255));
	  MapMenu->AddItem("I Can Fly", RGB(255, 0, 0), RGB(255,255, 255));
	  MapMenu->AddItem("Classic", RGB(255, 0, 0), RGB(255,255, 255));



}

void cdx_DoMapMenu()
{

    cdx_HandleMapMenuKeys();


    Screen->GetBack()->ChangeFont("Verdana", 0, MENU_ITEM_HEIGHT, FW_BOLD);
    Screen->GetBack()->SetFont();
    
    switch(MapMenu->Enter())
    {
        case 0:
            //new game logic
            //cdx_RestartGame();
            Map1->DrawTrans(Screen->GetBack());
            break;
        case 1:
            Map2->DrawTrans(Screen->GetBack());
            break;
        case 2:
            Map3->DrawTrans(Screen->GetBack());
            break;
    }

    

    //PlayerMenuLogo->Draw(Screen->GetBack( ), 0,	0, CDXBLT_BLK);
    MapMenu->Draw(100,300);



}


void cdx_HandleMapMenuKeys()
{
    //MainMenu->Home();
    if(Input->GetKeyState(CDXKEY_UPARROW)==CDXKEY_PRESS)
    {
        MapMenu->Up();
    }
    
    if(Input->GetKeyState(CDXKEY_DOWNARROW)==CDXKEY_PRESS)
    {
        MapMenu->Down();
    }

    if(Input->GetKeyState(CDXKEY_ENTER)==CDXKEY_PRESS)
    {
        switch(MapMenu->Enter())
        {
        case 0:
            //new game logic
            //cdx_RestartGame();
            cdx_DeInitMapMenu(1);
            break;
        case 1:
            cdx_DeInitMapMenu(2);
            break;
        case 2:
            cdx_DeInitMapMenu(3); 
            break;

        }
    }


}

void cdx_DeInitMapMenu(int mapno)
{
  // SAFEDELETE(BigFont);
    
    delete Map;

    if(mapno==1)
        Map=Map1;
    if(mapno==2)
        Map=Map2;
    if(mapno==3)
        Map=Map3;

     g_TileAnimator->SetMap(Map);
     g_TileAnimator2->SetMap(Map);
    
    
    if(mapno!=1)
        delete Map1;
    if(mapno!=2)
        delete Map2;
    if(mapno!=3)
        delete Map3;
        



    //SAFEDELETE(MapMenu);

    g_ProgramState=PS_MENU;
    Input->Update();
    
}

