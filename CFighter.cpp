// CDXDSprite.cpp: implementation of the CDXDSprite class.
//
//////////////////////////////////////////////////////////////////////

#include "CFighter.h"
#include "FIGHTERX.h"
#include "String.h"
#include "CDXHPC.h"

extern CDXHPC *timer;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#pragma warning ( disable: 4244 )

extern CDXScreen* Screen;

CFighter::CFighter()
:CDXSprite()
{
    //m_iFrames=0;
    //m_iStartFrame=0;    
    m_iCurFrame=0;
    m_fsState=FS_STANCE;
    m_iWalkVelocity=8;

    m_ptJumpVelo.x=12;
    m_ptJumpVelo.y=21;//30;//23

    m_iGravity=1;
    m_iGVelo=0;

    m_iPower=100;
    
    //SetRect(&m_rcCollision,75,100,250,41);
    SetRect(&m_rcCollision,0,0,0,0);


    m_iFrameTrigger=0;
    m_StanceData.iDelay=0;
    m_WalkfData.iDelay=0;
    m_WalkbData.iDelay=0;
    m_JumpuData.iDelay=0;
    m_JumpufData.iDelay=0;


    strcpy(m_szName,"");

   

}

CFighter::~CFighter()
{

}


/*
void CFighter::SetStartFrame(int num)
{
    m_iStartFrame=num;
    m_iCurFrame=num;
}


void CFighter::SetCurFrame(int num)
{
    m_iCurFrame=num;
    SetFrame(m_iCurFrame);
}
*/

void CFighter::SetFighterStateData(FIGHTERSTATE State,STATEDATA StateData)
{
    if(State==FS_STANCE)
    {
        m_StanceData=StateData;
        m_LandingData=m_StanceData;
    }
    if(State==FS_WALKF)
        m_WalkfData=StateData;
    if(State==FS_WALKB)
        m_WalkbData=StateData;
    if(State==FS_JUMPU)
        m_JumpuData=StateData;
    if(State==FS_JUMPUF)
        m_JumpufData=StateData;
    if(State==FS_JUMPUB)
        m_JumpubData=StateData;
    if(State==FS_CROUCH)
        m_CrouchData=StateData;
    if(State==FS_FALLING)
        m_FallingData=StateData;
    
    if(State==FS_PUNCH)
        m_PunchData=StateData;
    if(State==FS_PUNCHU)
        m_PunchuData=StateData;
    if(State==FS_PUNCHUF)
        m_PunchufData=StateData;
    if(State==FS_PUNCHC)
        m_PunchcData=StateData;

    if(State==FS_KICK)
        m_KickData=StateData;
    if(State==FS_KICKU)
        m_KickuData=StateData;
    if(State==FS_KICKUF)
        m_KickufData=StateData;
    if(State==FS_KICKC)
        m_KickcData=StateData;



    if(State==FS_HADOKEN)
        m_HadokenData=StateData;



    


}

BOOL CFighter::StatePossible(FIGHTERSTATE State)
{
    if(m_fsState==FS_JUMPU)
    {
        if(State==FS_STANCE||State==FS_CROUCH||State==FS_FALLING
            ||State==FS_HADOKEN||State==FS_PUNCHC||State==FS_KICKC)
            return FALSE;

        if(State==FS_WALKF||State==FS_JUMPUF)
        {
            ForceState(FS_JUMPUF);
            return FALSE;
        }
        if(State==FS_WALKB||State==FS_JUMPUB)
        {
            ForceState(FS_JUMPUB);
            return FALSE;
        }
        
        if(State==FS_PUNCH||State==FS_PUNCHU)
        {
            ForceState(FS_PUNCHU);
            return FALSE;
        }

        if(State==FS_KICK||State==FS_KICKU)
        {
            ForceState(FS_KICKU);
            return FALSE;
        }


        if(State==FS_PUNCHUF)
        {
            ForceState(FS_PUNCHUF);
            return FALSE;
        }

        if(State==FS_KICKUF)
        {
            ForceState(FS_KICKUF);
            return FALSE;
        }


    }

    if(m_fsState==FS_JUMPUF)
    {
        if(State==FS_WALKF||State==FS_FALLING||State==FS_HADOKEN
            ||State==FS_PUNCHC)
            return FALSE;
        
        if(State==FS_STANCE||State==FS_JUMPU||State==FS_CROUCH)
        {
            ForceState(FS_JUMPU);
            return FALSE;
        }
        if(State==FS_WALKB||State==FS_JUMPUB)
        {
            ForceState(FS_JUMPUB);
            return FALSE;
        }
        
        if(State==FS_PUNCH||State==FS_PUNCHU||State==FS_PUNCHUF)
        {
            ForceState(FS_PUNCHUF);
            return FALSE;
        }

        if(State==FS_KICK||State==FS_KICKU||State==FS_KICKUF)
        {
            ForceState(FS_KICKUF);
            return FALSE;
        }
        


    }

    if(m_fsState==FS_JUMPUB)
    {
        if(State==FS_WALKB||State==FS_FALLING
            ||State==FS_HADOKEN
            ||State==FS_PUNCHC
            ||State==FS_KICKC)
            return FALSE;
        
        if(State==FS_STANCE||State==FS_JUMPU||State==FS_CROUCH)
        {
            ForceState(FS_JUMPU);
            return FALSE;
        }

        if(State==FS_WALKF||State==FS_JUMPUF)
        {
            ForceState(FS_JUMPUF);
            return FALSE;
        }

        if(State==FS_PUNCHU||State==FS_PUNCH)
        {
            ForceState(FS_PUNCHU);
            return FALSE;
        }
        if(State==FS_KICKU||State==FS_KICK)
        {
            ForceState(FS_KICKU);
            return FALSE;
        }

    }


    if(m_fsState==FS_FALLING)
    {
        if(State==FS_STANCE||State==FS_HADOKEN)
            return FALSE;
        
        if(State==FS_JUMPU||State==FS_CROUCH||State==FS_PUNCHC
            ||State==FS_KICKC )
        {
        //    ForceState(FS_FALLING);
            return FALSE;
        }

        if(State==FS_WALKF||State==FS_JUMPUF)
        {
            ForceState(FS_JUMPUF);
            return FALSE;
        }

        if(State==FS_WALKB||State==FS_JUMPUB)
        {             
           
           ForceState(FS_JUMPUB);
           return FALSE;
        }
        if(State==FS_PUNCHU||State==FS_PUNCH)
        {
            ForceState(FS_PUNCHU);
            return FALSE;
        }
        
        if(State==FS_KICKU||State==FS_KICK)
        {
            ForceState(FS_KICKU);
            return FALSE;
        }
        
        if(State==FS_PUNCHUF)
        {
            ForceState(FS_PUNCHUF);
            return FALSE;
        }
        if(State==FS_KICKUF)
        {
            ForceState(FS_KICKUF);
            return FALSE;
        }

               
    }

    if(m_fsState==FS_LANDING)
         return FALSE;

    
    if(m_fsState==FS_PUNCHU)
    {
        if(State==FS_STANCE||State==FS_FALLING
            ||State==FS_JUMPU||State==FS_PUNCH||State==FS_HADOKEN
            ||State==FS_CROUCH)
            return FALSE;

        if(State==FS_WALKF||State==FS_JUMPUF)
        {
            ForceState(FS_PUNCHUF);
            return FALSE;
        }

        if(State==FS_WALKB||State==FS_JUMPUB)
        {
            ForceState(FS_JUMPUB);
            return FALSE;
        }  
        
        if(State==FS_PUNCHUF)
        {
            ForceState(FS_PUNCHUF);
            return FALSE;
        }

        if(State==FS_KICKUF)
        {
            //ForceState(FS_KICKUF);
            return FALSE;
        }


        if(State==FS_KICK||State==FS_KICKU)
        {
            //ForceState(FS_KICKU);
            return FALSE;
        }
        

    }

    if(m_fsState==FS_KICKU)
    {
        if(State==FS_STANCE||State==FS_FALLING
            ||State==FS_JUMPU||State==FS_KICK||State==FS_HADOKEN
            ||State==FS_CROUCH)
            return FALSE;

        if(State==FS_WALKF||State==FS_JUMPUF)
        {
            ForceState(FS_KICKUF);
            return FALSE;
        }

        if(State==FS_WALKB||State==FS_JUMPUB)
        {
            ForceState(FS_JUMPUB);
            return FALSE;
        }  
        
        if(State==FS_KICKUF)
        {
            ForceState(FS_KICKUF);
            return FALSE;
        }

        if(State==FS_PUNCHUF)
        {
            //ForceState(FS_PUNCHUF);
            return FALSE;
        }


        if(State==FS_PUNCH||State==FS_PUNCHU)
        {
           // ForceState(FS_PUNCHU);
            return FALSE;
        }
        

    }


    if(m_fsState==FS_PUNCHUF)
    {
        if(State==FS_WALKF||State==FS_CROUCH||State==FS_FALLING
            ||State==FS_PUNCHUF||FS_JUMPUF||State==FS_HADOKEN||State==FS_PUNCHC)
            return FALSE;
        
        
        if(State==FS_STANCE||State==FS_JUMPU||State==FS_CROUCH)
        {
            //ForceState(FS_JUMPU);
            return FALSE;
        }


        if(State==FS_WALKB||State==FS_JUMPUB)
        {
            ForceState(FS_JUMPUB);
            return FALSE;
        }
        
        if(State==FS_PUNCHU||State==FS_PUNCH)
        {
            ForceState(FS_PUNCHU);
            return FALSE;
        }

        if(State==FS_KICKU||State==FS_KICK)
        {
            //ForceState(FS_KICKU);
            return FALSE;
        }
        
        if(State==FS_KICKUF)
        {
            //ForceState(FS_KICKUF);
            return FALSE;
        }
    }
    
    if(m_fsState==FS_KICKUF)
    {
        if(State==FS_WALKF||State==FS_CROUCH||State==FS_FALLING
            ||State==FS_KICKUF||FS_JUMPUF||State==FS_HADOKEN||State==FS_PUNCHC)
            return FALSE;
        
        
        if(State==FS_STANCE||State==FS_JUMPU||State==FS_CROUCH)
        {
            //ForceState(FS_JUMPU);
            return FALSE;
        }


        if(State==FS_WALKB||State==FS_JUMPUB)
        {
            ForceState(FS_JUMPUB);
            return FALSE;
        }
        
        if(State==FS_PUNCHU||State==FS_PUNCH)
        {
            //ForceState(FS_PUNCHU);
            return FALSE;
        }

        if(State==FS_KICKU||State==FS_KICK)
        {
            ForceState(FS_KICKU);
            return FALSE;
        }
        
        if(State==FS_PUNCHUF)
        {
            //ForceState(FS_PUNCHUF);
            return FALSE;
        }
    }


    if(State==FS_HADOKEN)
    {
        if(!m_pHadokenFire->IsHidden())
            return FALSE;        
    }

    
    if(m_fsState==FS_HADOKEN)
    {
        if(State==FS_STANCE)
            return FALSE;
    }

    if(m_fsState==FS_PUNCHC)
    {
        if(State==FS_CROUCH)
            return FALSE;

        if(State==FS_JUMPU)
        {
            ForceState(FS_PUNCHU);
            return FALSE;
        }
        if(State==FS_JUMPUF)
        {
            ForceState(FS_PUNCHUF);
            return FALSE;
        }
    }


    
    if(m_fsState==FS_KICKC)
    {
        if(State==FS_CROUCH)
            return FALSE;

        if(State==FS_JUMPU)
        {
            ForceState(FS_KICKU);
            return FALSE;
        }
        if(State==FS_JUMPUF)
        {
            ForceState(FS_KICKUF);
            return FALSE;
        }
    }

    if(m_fsState==FS_HIT)
        return FALSE;


    if(m_fsState==FS_PUNCH)
    {
        if(State==FS_STANCE||
            State==FS_KICK||State==FS_KICKU||State==FS_KICKUF)
         return FALSE;
    }
    
    if(m_fsState==FS_KICK)
    {
        if(State==FS_STANCE||
            State==FS_PUNCH||State==FS_PUNCHU||State==FS_PUNCHUF)
         return FALSE;
    }
    


    return TRUE;
}


void CFighter::ForceState(FIGHTERSTATE State)
{
    //TEMP
    //Update();

    
    
    
    
    m_fsState=State;


    
    if(State==FS_STANCE)
    {
        m_iCurFrame=m_StanceData.iStartFrame;
    }
    
    if(State==FS_WALKF)
    {
        m_iCurFrame=m_WalkfData.iStartFrame;
    }

    if(State==FS_WALKB)
    {
        m_iCurFrame=m_WalkbData.iStartFrame;
    }

    if(State==FS_JUMPU)
    {
        m_iCurFrame=m_JumpuData.iStartFrame;
        
    }

    if(State==FS_JUMPUF)
    {
        m_iCurFrame=m_JumpufData.iStartFrame;    
    }

    if(State==FS_JUMPUB)
    {
        m_iCurFrame=m_JumpubData.iStartFrame;    
    }

    if(State==FS_CROUCH)
    {
        m_iCurFrame=m_CrouchData.iStartFrame;    
    }

    if(State==FS_FALLING)
    {
        m_iCurFrame=m_FallingData.iStartFrame;    
        
        //temp
       //m_iGVelo=m_ptJumpVelo.y;
    }
    if(State==FS_LANDING)
    {
        m_iCurFrame=m_LandingData.iStartFrame;
        m_iGVelo=m_ptJumpVelo.y;
    }

    if(State==FS_PUNCH)
    {
        m_iCurFrame=m_PunchData.iStartFrame;        
    }

    if(State==FS_PUNCHU)
    {
        m_iCurFrame=m_PunchuData.iStartFrame;        
    }

    if(State==FS_PUNCHUF)
    {
        m_iCurFrame=m_PunchufData.iStartFrame;        
    }
    
    if(State==FS_PUNCHC)
    {
        m_iCurFrame=m_PunchcData.iStartFrame;
    }

    if(State==FS_KICK)
    {
        m_iCurFrame=m_KickData.iStartFrame;        
    }

    if(State==FS_KICKU)
    {
        m_iCurFrame=m_KickuData.iStartFrame;        
    }

    if(State==FS_KICKUF)
    {
        m_iCurFrame=m_KickufData.iStartFrame;        
    }

    if(State==FS_KICKC)
    {
        m_iCurFrame=m_KickcData.iStartFrame;
    }

    
    if(State==FS_HADOKEN)
    {
        m_iCurFrame=m_HadokenData.iStartFrame;      
        
        
    }
    
    if(State==FS_HIT)
    {
        m_iCurFrame=m_JumpubData.iStartFrame;
    }


    //Update();
    
}



BOOL CFighter::SetFighterState(FIGHTERSTATE State)
{
    if(m_fsState==State)
        return FALSE;
    
    //cdx_CheckMapCollision();
    

    if(!StatePossible(State))
        return FALSE;

    m_iGVelo=0;
   

    ForceState(State);   
    

    
    
    return TRUE;
}





void CFighter::Update()
{
   
    UpdateFire();

    

    if (--m_iFrameTrigger <= 0)
    {
        // Reset the frame trigger;
        m_iFrameTrigger=0;
            
        if(m_fsState==FS_STANCE)
        {
                
    
            if(  (++m_iCurFrame-m_StanceData.iStartFrame)==m_StanceData.iFrames)
            {
                m_iCurFrame=m_StanceData.iStartFrame;
                
            }   
            m_iFrameTrigger = m_StanceData.iDelay;
            CheckFalling();
            return;
        }
        
        if(m_fsState==FS_WALKF)
        {
            if(  (++m_iCurFrame-m_WalkfData.iStartFrame)==m_WalkfData.iFrames)
            {
                m_iCurFrame=m_WalkfData.iStartFrame;
                
            }   
            m_iFrameTrigger = m_WalkfData.iDelay;            
            SetPosX(GetPosX()+m_iWalkVelocity);
            CheckFalling();
            return;
        }
    
        
        if(m_fsState==FS_WALKB)
        {
            if(  (++m_iCurFrame-m_WalkbData.iStartFrame)==m_WalkbData.iFrames)
            {
                m_iCurFrame=m_WalkbData.iStartFrame;
                
            }   
            m_iFrameTrigger = m_WalkbData.iDelay;
            
            SetPosX(GetPosX()-m_iWalkVelocity);
            CheckFalling();
            return;
        }
    
        //GRAVITY
        m_iGVelo+=m_iGravity;
        //GRAV END
    
        
        if(m_fsState==FS_JUMPU)
        {
            if(  (++m_iCurFrame-m_JumpuData.iStartFrame)==m_JumpuData.iFrames)
            {
                m_iCurFrame=m_JumpuData.iStartFrame;
                
            }   

            m_iFrameTrigger = m_JumpuData.iDelay;
            
            if(CheckUp())
            {
                 SetPosY(GetPosY()-m_ptJumpVelo.y+m_iGVelo);
            }

            
        }

        if(m_fsState==FS_JUMPUF)
        {
            if(  (++m_iCurFrame-m_JumpufData.iStartFrame)==m_JumpufData.iFrames)
            {
                m_iCurFrame=m_JumpufData.iStartFrame;
                
            }   
            m_iFrameTrigger = m_JumpufData.iDelay;
            
            if(CheckUp())
            {
                SetPosY(GetPosY()-m_ptJumpVelo.y+m_iGVelo);
            }
            if(!CheckRight())
            {
                SetPosX(GetPosX()+m_ptJumpVelo.x);
            }
        }

        if(m_fsState==FS_JUMPUB)
        {
            if(  (++m_iCurFrame-m_JumpubData.iStartFrame)==m_JumpubData.iFrames)
            {
                m_iCurFrame=m_JumpubData.iStartFrame;
                
            }   
            m_iFrameTrigger = m_JumpubData.iDelay;
            
            if(CheckUp())
            {
                SetPosY(GetPosY()-m_ptJumpVelo.y+m_iGVelo);
            }
            
            if(!CheckLeft())
            {
                SetPosX(GetPosX()-m_ptJumpVelo.x);
            }

        }

        if(m_fsState==FS_CROUCH)
        {
                
    
            if(  (++m_iCurFrame-m_CrouchData.iStartFrame)==m_CrouchData.iFrames)
            {
                m_iCurFrame=m_CrouchData.iStartFrame;
                
            }   
            m_iFrameTrigger = m_CrouchData.iDelay;
            CheckFalling();
            return;
        }

        
        if(m_fsState==FS_FALLING)
        {
            if(  (++m_iCurFrame-m_FallingData.iStartFrame)==m_FallingData.iFrames)
            {
                m_iCurFrame=m_FallingData.iStartFrame;
                
            }   

            m_iFrameTrigger = m_FallingData.iDelay;
            
            SetPosY(GetPosY()-m_ptJumpVelo.y+m_iGVelo);
            //CheckFalling();
           // return;
        }

        if(m_fsState==FS_LANDING)
        {
            if(  (++m_iCurFrame-m_LandingData.iStartFrame)==m_LandingData.iFrames)
            {
                m_iCurFrame=m_LandingData.iStartFrame;
                
            }   

            m_iFrameTrigger = m_LandingData.iDelay;
            
            //if(CheckDown())
            //{
              SetPosY(GetPosY()-m_ptJumpVelo.y+m_iGVelo);
           // }
           // else
            //    ForceState(FS_STANCE);  
            CheckFalling();
            return;
        }

        
        if(m_fsState==FS_PUNCH)
        {
                
    
            if(  (++m_iCurFrame-m_PunchData.iStartFrame)==m_PunchData.iFrames)
            {
                m_iCurFrame=m_PunchData.iStartFrame;
                ForceState(FS_STANCE);
                return;
                
            }   
            m_iFrameTrigger = m_PunchData.iDelay;
            CheckFalling();
            return;
        }

        if(m_fsState==FS_PUNCHU)
        {
            if(  (++m_iCurFrame-m_PunchuData.iStartFrame)==m_PunchuData.iFrames)
            {
                m_iCurFrame=m_PunchuData.iStartFrame;
                ForceState(FS_FALLING);
                return;
            }   

            m_iFrameTrigger = m_PunchuData.iDelay;
            
            if(CheckUp())
            {
                 SetPosY(GetPosY()-m_ptJumpVelo.y+m_iGVelo);
            }
        }

        if(m_fsState==FS_PUNCHUF)
        {
            if(  (++m_iCurFrame-m_PunchufData.iStartFrame)==m_PunchufData.iFrames)
            {
                m_iCurFrame=m_PunchufData.iStartFrame;
                ForceState(FS_FALLING);
                return;
                
            }   
            m_iFrameTrigger = m_PunchufData.iDelay;
            
            if(CheckUp())
            {
                SetPosY(GetPosY()-m_ptJumpVelo.y+m_iGVelo);
            }
            if(!CheckRight())
            {
                SetPosX(GetPosX()+m_ptJumpVelo.x);
            }
        }

        
        if(m_fsState==FS_PUNCHC)
        {
                
    
            if(  (++m_iCurFrame-m_PunchcData.iStartFrame)==m_PunchcData.iFrames)
            {
                m_iCurFrame=m_PunchcData.iStartFrame;
                ForceState(FS_CROUCH);
                return;
                
            }   
            m_iFrameTrigger = m_PunchData.iDelay;
            CheckFalling();
            return;
        }        

        if(m_fsState==FS_KICK)
        {
                
    
            if(  (++m_iCurFrame-m_KickData.iStartFrame)==m_KickData.iFrames)
            {
                m_iCurFrame=m_KickData.iStartFrame;
                ForceState(FS_STANCE);
                return;
                
            }   
            m_iFrameTrigger = m_KickData.iDelay;
            CheckFalling();
            return;
        }

        if(m_fsState==FS_KICKU)
        {
            if(  (++m_iCurFrame-m_KickuData.iStartFrame)==m_KickuData.iFrames)
            {
                m_iCurFrame=m_KickuData.iStartFrame;
                ForceState(FS_FALLING);
                return;
            }   

            m_iFrameTrigger = m_KickuData.iDelay;
            
            if(CheckUp())
            {
                 SetPosY(GetPosY()-m_ptJumpVelo.y+m_iGVelo);
            }
        }

        if(m_fsState==FS_KICKUF)
        {
            if(  (++m_iCurFrame-m_KickufData.iStartFrame)==m_KickufData.iFrames)
            {
                m_iCurFrame=m_KickufData.iStartFrame;
                ForceState(FS_FALLING);
                return;
                
            }   
            m_iFrameTrigger = m_KickufData.iDelay;
            
            if(CheckUp())
            {
                SetPosY(GetPosY()-m_ptJumpVelo.y+m_iGVelo);
            }
            if(!CheckRight())
            {
                SetPosX(GetPosX()+m_ptJumpVelo.x);
            }
        }

        
        if(m_fsState==FS_KICKC)
        {
                
    
            if(  (++m_iCurFrame-m_KickcData.iStartFrame)==m_KickcData.iFrames)
            {
                m_iCurFrame=m_KickcData.iStartFrame;
                ForceState(FS_CROUCH);
                return;
                
            }   
            m_iFrameTrigger = m_KickData.iDelay;
            CheckFalling();
            return;
        }        

        if(m_fsState==FS_HADOKEN)
        {
                
    
            if(  (++m_iCurFrame-m_HadokenData.iStartFrame)==m_HadokenData.iFrames)
            {
                m_iCurFrame=m_HadokenData.iStartFrame;
                ForceState(FS_STANCE);
                return;
                
            }   
            if((m_iCurFrame-m_HadokenData.iStartFrame)==m_iHadokenCount)
            {
                m_pHadokenFire->SetHidden(FALSE);
                m_pHadokenFire->SetPos(GetPosX()+m_iHadokenX,GetPosY()+m_iHadokenY);
                SetPower(GetPower()-1);
            }

            m_iFrameTrigger = m_HadokenData.iDelay;


            CheckFalling();
            return;
        }
    

        if(m_fsState==FS_HIT)
        {
            if(  (++m_iCurFrame-m_JumpubData.iStartFrame)==m_JumpubData.iFrames)
            {
                m_iCurFrame=m_JumpubData.iStartFrame;
                ForceState(FS_STANCE);
                return;
                
            }   
            m_iFrameTrigger = m_JumpubData.iDelay;
            
            if(CheckUp())
            {
                SetPosY(GetPosY()-m_ptJumpVelo.y+m_iGVelo);
            }
            
            if(!CheckLeft())
            {
                SetPosX(GetPosX()-m_ptJumpVelo.x);
            }

        }


        
        SetFrame(m_iCurFrame);
        cdx_CheckMapCollision(this);
        //CheckFalling();
    
    }
}


void CFighter::CheckFalling()
{
        SetFrame(m_iCurFrame);
        cdx_CheckMapCollision(this);
        
        
        SetPosY(GetPosY()+m_ptJumpVelo.y);
        if(cdx_CheckFloorCollision(this))
        {//WE are safe......
            SetPosY(GetPosY()-m_ptJumpVelo.y);            
            return;
        }
        else
        {//Oops you missed the Ground....
             SetPosY(GetPosY()-m_ptJumpVelo.y);            
             m_iGVelo=m_ptJumpVelo.y;
             ForceState(FS_FALLING);
        }
}

BOOL CFighter::CheckLeft()
{
        SetPosX(GetPosX()-m_ptJumpVelo.x);
     
        if(cdx_CheckFloorCollision(this))
        {//Sorry we cant go there......
            SetPosX(GetPosX()+m_ptJumpVelo.x);            
            return TRUE;
        }
        else
        {//Yup you can go there
             SetPosX(GetPosX()+m_ptJumpVelo.x);            
             return FALSE;
        }
}


BOOL CFighter::CheckRight()
{
        
        SetPosX(GetPosX()+m_ptJumpVelo.x);
     
        if(cdx_CheckFloorCollision(this))
        {//Sorry we cant go there......
            SetPosX(GetPosX()-m_ptJumpVelo.x);            
            return TRUE;
        }
        else
        {//Yup you can go there
             SetPosX(GetPosX()-m_ptJumpVelo.x);            
             return FALSE;
        }
}

BOOL CFighter::CheckUp()
{
        
    if(m_ptJumpVelo.y>m_iGVelo)
    {
        SetPosY(GetPosY()-m_ptJumpVelo.y+m_iGVelo);
     
        if(cdx_CheckFloorCollision(this))
        {//Sorry we cant go there......
            SetPosY(GetPosY()+m_ptJumpVelo.y-m_iGVelo);            
            m_iGVelo=m_ptJumpVelo.y;
            return FALSE;
        }
        else
        {//Yup you can go there
             SetPosY(GetPosY()+m_ptJumpVelo.y-m_iGVelo);            
             return TRUE;
        }
    }
    else 
        return TRUE;

}

BOOL CFighter::CheckDown()
{
    if(m_ptJumpVelo.y<m_iGVelo)
    {
    
       SetPosY(GetPosY()-m_ptJumpVelo.y+m_iGVelo);
     
        if(cdx_CheckFloorCollision(this))
        {//Sorry we cant go there......
            SetPosY(GetPosY()+m_ptJumpVelo.y-m_iGVelo);            
            m_iGVelo=m_ptJumpVelo.y;
            return FALSE;
        }
        else
        {//Yup you can go there
             SetPosY(GetPosY()+m_ptJumpVelo.y-m_iGVelo);            
             return TRUE;
        }
    }
    else
        return TRUE;
}


void CFighter::Reverse()
{

    if(m_fsState==FS_WALKF)
    {
        SetPosX(GetPosX()-m_iWalkVelocity);
        
    }
    if(m_fsState==FS_WALKB)
    {
        SetPosX(GetPosX()+m_iWalkVelocity);        
    }
    
    if(m_fsState==FS_JUMPUF)
    {
        SetPosX(GetPosX()-m_ptJumpVelo.x);
        //ForceState(FS_JUMPU);
    }

    if(m_fsState==FS_JUMPUB)
    {
        SetPosX(GetPosX()+m_ptJumpVelo.x);
        //ForceState(FS_JUMPU);
    }

    if(m_fsState==FS_PUNCHUF)
    {
        SetPosX(GetPosX()-m_ptJumpVelo.x);
    }

    if(m_fsState==FS_KICKUF)
    {
        SetPosX(GetPosX()-m_ptJumpVelo.x);
    }

    if(m_fsState==FS_HIT)
    {
        SetPosX(GetPosX()+m_ptJumpVelo.x);
        //ForceState(FS_JUMPU);
    }


    //Update();
    

}

void CFighter::Floor()
{

    if(m_fsState==FS_WALKF)
    {
        SetPosX(GetPosX()-m_iWalkVelocity);
        ForceState(FS_LANDING);
        //CDXError(Screen,"WE ARE HERER");
        
    }
    if(m_fsState==FS_WALKB)
    {
        SetPosX(GetPosX()+m_iWalkVelocity);        
        ForceState(FS_LANDING);
        
    }
    

    if(m_fsState==FS_JUMPU)
    {
        SetPosY(GetPosY()+m_ptJumpVelo.y-m_iGVelo);
        ForceState(FS_LANDING);
    }

    if(m_fsState==FS_JUMPUF)
    {
        SetPosX(GetPosX()-m_ptJumpVelo.x);
        SetPosY(GetPosY()+m_ptJumpVelo.y-m_iGVelo);
        ForceState(FS_LANDING);
    }

    
    if(m_fsState==FS_JUMPUB)
    {
        SetPosY(GetPosY()+m_ptJumpVelo.y-m_iGVelo);
        SetPosX(GetPosX()+m_ptJumpVelo.x);
        ForceState(FS_LANDING);
    }

    if(m_fsState==FS_FALLING)
    {
        SetPosY(GetPosY()+m_ptJumpVelo.y-m_iGVelo);
        ForceState(FS_STANCE);
    }

    if(m_fsState==FS_LANDING)
    {
        SetPosY(GetPosY()+m_ptJumpVelo.y-m_iGVelo);
        ForceState(FS_STANCE);
    }

    if(m_fsState==FS_PUNCHU)
    {
        SetPosY(GetPosY()+m_ptJumpVelo.y-m_iGVelo);
        int temp=m_iGVelo;
        ForceState(FS_LANDING);
        m_iGVelo=temp;
    }

    
    if(m_fsState==FS_PUNCHUF)
    {
        SetPosX(GetPosX()-m_ptJumpVelo.x);
        SetPosY(GetPosY()+m_ptJumpVelo.y-m_iGVelo);
        int temp=m_iGVelo;
        ForceState(FS_LANDING);
        m_iGVelo=temp;
    }
    
    if(m_fsState==FS_KICKU)
    {
        SetPosY(GetPosY()+m_ptJumpVelo.y-m_iGVelo);
        int temp=m_iGVelo;
        ForceState(FS_LANDING);
        m_iGVelo=temp;
    }

    
    if(m_fsState==FS_KICKUF)
    {
        SetPosX(GetPosX()-m_ptJumpVelo.x);
        SetPosY(GetPosY()+m_ptJumpVelo.y-m_iGVelo);
        int temp=m_iGVelo;
        ForceState(FS_LANDING);
        m_iGVelo=temp;
    }


    
    if(m_fsState==FS_HIT)
    {
        SetPosY(GetPosY()+m_ptJumpVelo.y-m_iGVelo);
        SetPosX(GetPosX()+m_ptJumpVelo.x);
        ForceState(FS_LANDING);
    }


    //Update();
}



void CFighter::SetCollisionRect(RECT CollRect)
{
    SetRect(&m_rcCollision,CollRect.left,CollRect.top,CollRect.right,CollRect.bottom);
}


BOOL CFighter::TileHitEx(CDXMap* pMap, int Tile)
{

   // if(!TileHit(pMap,Tile))
    //{
     //   return FALSE;
    //}

    //cdx_ShowTiles();

	  double MapX
        ,MapY
        , TileType;


    // Check top left
    MapX = (pMap->GetPosX() + GetPosX()+m_rcCollision.left )*1.0 / pMap->GetTileWidth();
    MapY = (pMap->GetPosY() + GetPosY()+m_rcCollision.top )*1.0 / pMap->GetTileHeight();
    TileType = pMap->GetTile(min(MapX, pMap->GetMapWidth()-1), min(MapY, pMap->GetMapHeight()-1));
    if(TileType == Tile) return TRUE;

   // else
   // {
        //TileType = pMap->GetTile(min(MapX,pMap->GetMapWidth()-1), min(MapY+.5, pMap->GetMapHeight()-1));
        //if(TileType == Tile) return TRUE;
    //}


    // Check top right
    MapX = (pMap->GetPosX() + GetPosX() + GetTile()->m_BlockWidth-m_rcCollision.right)*1.0 / pMap->GetTileWidth();
    MapY = (pMap->GetPosY() + GetPosY()+m_rcCollision.top )*1.0 / pMap->GetTileHeight();
    TileType = pMap->GetTile(min(MapX, pMap->GetMapWidth()-1), min(MapY, pMap->GetMapHeight()-1));
    if(TileType == Tile) return TRUE;

    //else
    //{
     //  TileType = pMap->GetTile(min(MapX, pMap->GetMapWidth()-1), min(MapY-1.5, pMap->GetMapHeight()-1));
     //  if(TileType == Tile) return TRUE;
    //}


    // Check bottom left
    MapX = (pMap->GetPosX() + GetPosX()+m_rcCollision.left )*1.0/ pMap->GetTileWidth();
    MapY = (pMap->GetPosY() + GetPosY() + GetTile()->m_BlockHeight-m_rcCollision.bottom)*1.0 / pMap->GetTileHeight();
    TileType = pMap->GetTile(min(MapX, pMap->GetMapWidth()-1), min(MapY, pMap->GetMapHeight()-1));
    if(TileType == Tile) return TRUE;
    else
    {
       TileType = pMap->GetTile(min(MapX, pMap->GetMapWidth()-1), min(MapY-1,pMap->GetMapHeight()-1));
       if(TileType == Tile) return TRUE;
    }
    


    // Check bottom right
    MapX = (pMap->GetPosX() + GetPosX() + GetTile()->m_BlockWidth-m_rcCollision.right) *1.0/ pMap->GetTileWidth();
    MapY = (pMap->GetPosY() + GetPosY() + GetTile()->m_BlockHeight-m_rcCollision.bottom) *1.0/ pMap->GetTileHeight();
    TileType = pMap->GetTile(min(MapX, pMap->GetMapWidth()-1), min(MapY, pMap->GetMapHeight()-1));
    if(TileType == Tile) return TRUE;
    else
    {
        TileType = pMap->GetTile(min(MapX, pMap->GetMapWidth()-1), min(MapY-1,pMap->GetMapHeight()-1));
        if(TileType == Tile) return TRUE;
    }

    //Check Left...
   //  MapX=(MapX1);//+MapX3)/2;
   // MapY=(MapY1);//+MapY3)/2;
   //  TileType = pMap->GetTile(min(MapX, pMap->GetMapWidth()-1), min(MapY, pMap->GetMapHeight()-1));
   //  if(TileType == Tile) return TRUE;
   //  else
   //  {
   //       TileType = pMap->GetTile(min(MapX,pMap->GetMapWidth()-1), min(MapY-1, pMap->GetMapHeight()-1));
   //       if(TileType == Tile) return TRUE;
   //  }
    
     


	return FALSE;
}




void CFighter::SetMap(CDXMap* Map)
{
    m_Map=Map;
}

void CFighter::SetFloorTile(int tile)
{
    m_iFloorTile=tile;
}


FIGHTERSTATE CFighter::GetFighterState()
{
    return m_fsState;
}

void CFighter::SetFire(CSprite * Hadoken,int Count,int xdisp,int ydisp)
{
    m_pHadokenFire=Hadoken;
    m_iHadokenCount=Count;
    m_iHadokenX=xdisp;
    m_iHadokenY=ydisp;

}

void CFighter::UpdateFire()
{
    if(m_pHadokenFire->IsHidden())
        return;
    
    m_pHadokenFire->Update();

}

/*
FS_STANCE   = 0,
                   FS_WALKF    = 1,
                   FS_WALKB    = 2,
                   FS_JUMPU    = 3,
                   FS_JUMPUF   = 4,
                   FS_JUMPUB   = 5,
                   FS_CROUCH   = 6,
                   FS_FALLING  = 7,
                   FS_LANDING  = 8,
                   FS_PUNCH    = 9,
                   FS_PUNCHU   = 10,
                   FS_PUNCHUF  = 11,
                   FS_PUNCHC   = 12,
                   FS_KICK     = 13,
                   FS_KICKU    = 14,
                   FS_KICKUF   = 15,
                   FS_KICKC    = 16,
                   FS_HADOKEN  = 17,
                   FS_HIT      = 18;   
                   
*/


BOOL CFighter::SetHit(FIGHTERSTATE HitterState)//FALSE MEANS NO HIT
{
    if(m_iImmunity==HitterState)
        return FALSE;

    if(m_fsState==FS_STANCE||m_fsState==FS_WALKF||m_fsState==FS_WALKB
        ||m_fsState==FS_JUMPU ||m_fsState==FS_JUMPUF ||m_fsState==FS_JUMPUB
        ||m_fsState==FS_CROUCH||m_fsState==FS_FALLING||m_fsState==FS_LANDING
        ||m_fsState==FS_PUNCH ||m_fsState==FS_PUNCHU ||m_fsState==FS_PUNCHUF
        ||m_fsState==FS_PUNCHC
        ||m_fsState==FS_KICK  ||m_fsState==FS_KICKU  ||m_fsState==FS_KICKUF
        ||m_fsState==FS_KICKC
       )
    {
        if(HitterState==FS_PUNCH||HitterState==FS_PUNCHU
            ||HitterState==FS_PUNCHUF||HitterState==FS_PUNCHC)
        {
            if(m_iImmunity==FS_PUNCH)
                return FALSE;

            SetFighterState(FS_HIT);
            SetPower(GetPower()-3);
            m_iImmunity=FS_PUNCH;
            return TRUE;
        }
        
        if(HitterState==FS_KICK||HitterState==FS_KICKU
            ||HitterState==FS_KICKUF||HitterState==FS_KICKC)
        {
            if(m_iImmunity==FS_KICK)
                return FALSE;

            SetFighterState(FS_HIT);
            SetPower(GetPower()-5);
            m_iImmunity=FS_KICK;
            return TRUE;
        }
    }
    
    if(m_fsState==FS_HADOKEN)
    {
        if(HitterState==FS_PUNCH||HitterState==FS_PUNCHU
            ||HitterState==FS_PUNCHUF||HitterState==FS_PUNCHC)
        {
            if(m_iImmunity==FS_PUNCH)
                return FALSE;
         
            SetFighterState(FS_HIT);
            SetPower(GetPower()-2*3);
            m_iImmunity=FS_PUNCH;
            return TRUE;
        }
        
        if(HitterState==FS_KICK||HitterState==FS_KICKU
            ||HitterState==FS_KICKUF||HitterState==FS_KICKC)
        {
            if(m_iImmunity==FS_KICK)
                return FALSE;

            SetFighterState(FS_HIT);
            SetPower(GetPower()-2*5);
            m_iImmunity=FS_KICK;
            return TRUE;
        }        

    }
	return FALSE;

}


DWORD CFighter::GetStateTime()
{
    return m_StateTimer;
}

void CFighter::SetStateTime(DWORD Time)
{
    m_StateTimer=Time;
}


void CFighter::SetName(char*name)
{
    strcpy(m_szName,name);
}
