#include "FighterX.h"
#include "CFighter.h"
#include "CSprite.h"


extern CDXScreen *Screen;

extern int CKEY_LEFTARROW,
           CKEY_RIGHTARROW,
           CKEY_BASLEFT,
           CKEY_BASRIGHT;

extern CFighter* g_FirstFighter,
       * g_FirstFighter1,
       * g_FirstFighter2;
extern CDXSprite*g_FirstSplash;


extern CFighter* g_SecondFighter,
       * g_SecondFighter1,
       * g_SecondFighter2;
extern CDXSprite*g_SecondSplash;


extern CDXSprite *g_BruceSplash;
extern CDXSprite *g_CyrusSplash;
extern CDXSprite *g_LadySplash;
extern CDXSprite *g_JuniorSplash;



void LoadLady(int PlayerNo)
{
    
    POINT JumpVelo;
    RECT Bounds;
    STATEDATA Data;
    RECT CollRect;
    
    

    CSprite*LadyFire1;
    CSprite*LadyFire2;

    SetRect(&Bounds,0,0,1024,768);

    CFighter *Lady,*Lady1,*Lady2;

    CDXSprite* LadySplash;


    /////////////////////////////////////////////////////////////
//Now it is time for the Lady to make its appearance in sys memory !!
/////////////////////////////////////////////////////////////////




  Lady1 = new CFighter( );

	if( Lady1 == NULL )
		CDXError( Screen , "Could not allocate sprites object!" );

	if( Lady1->Create( Screen , "./Gfx/BL1.bmp" , 187 , 230 , 195 , CDXMEM_SYSTEMONLY )<0 )
		CDXError( Screen , "Could not load sprites from BL1.bmp" );

	Lady1->SetColorKey( );
  Lady1->SetFighterState(FS_STANCE);
  
  
  Data.iFrames=11;
  Data.iStartFrame=0;
  Data.iDelay=0;
  Lady1->SetFighterStateData(FS_STANCE,Data);

  Data.iFrames=16;
  Data.iStartFrame=11;
  Data.iDelay=0;
  Lady1->SetFighterStateData(FS_WALKF,Data);

  Data.iFrames=16;
  Data.iStartFrame=27;
  Lady1->SetFighterStateData(FS_WALKB,Data);
  
  Data.iFrames=8;
  Data.iStartFrame=43;
  Lady1->SetFighterStateData(FS_JUMPU,Data);

   
  Data.iFrames=8;
  Data.iStartFrame=51;
  Lady1->SetFighterStateData(FS_JUMPUF,Data);


  Data.iFrames=8;
  Data.iStartFrame=59;
  Lady1->SetFighterStateData(FS_JUMPUB,Data);


  Data.iFrames=11;
  Data.iStartFrame=67;
  Data.iDelay=0;
  Lady1->SetFighterStateData(FS_CROUCH,Data);


  Data.iFrames=2;
  Data.iStartFrame=78;
  Data.iDelay=0;
  Lady1->SetFighterStateData(FS_FALLING,Data);

  
  Data.iFrames=12;
  Data.iStartFrame=80;
  Data.iDelay=0;
  Lady1->SetFighterStateData(FS_PUNCH,Data);


  Data.iFrames=20;
  Data.iStartFrame=92;
  Data.iDelay=0;
  Lady1->SetFighterStateData(FS_PUNCHU,Data);

  Data.iFrames=14;
  Data.iStartFrame=112;
  Data.iDelay=0;
  Lady1->SetFighterStateData(FS_PUNCHUF,Data);


  Data.iFrames=22;
  Data.iStartFrame=126;
  Data.iDelay=0;
  Lady1->SetFighterStateData(FS_HADOKEN,Data);


  Data.iFrames=7;
  Data.iStartFrame=148;
  Data.iDelay=0;
  Lady1->SetFighterStateData(FS_PUNCHC,Data);


  Data.iFrames=14;
  Data.iStartFrame=155;
  Data.iDelay=0;
  Lady1->SetFighterStateData(FS_KICK,Data);


  Data.iFrames=13;
  Data.iStartFrame=169;
  Data.iDelay=0;
  Lady1->SetFighterStateData(FS_KICKU,Data);

  Data.iFrames=8;
  Data.iStartFrame=182;
  Data.iDelay=0;
  Lady1->SetFighterStateData(FS_KICKUF,Data);

  Data.iFrames=5;
  Data.iStartFrame=190;
  Data.iDelay=0;
  Lady1->SetFighterStateData(FS_KICKC,Data);


  Data.iDelay=0;


  
  Lady1->SetWalkVelocity(10);
  JumpVelo.x= 14;
  JumpVelo.y=  24;
  Lady1->SetJumpVelocity(JumpVelo);


  
  SetRect(&CollRect,45,100,80,40);
  Lady1->SetCollisionRect(CollRect);

  Lady1->SetShadowOffset(100,35);
	

  Lady1->SetPos(350,503);

//  InitPreloader(InitLoader+=10);

/////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
  //What is a fighter without firepower??
  LadyFire1=new CSprite();
  
  if(LadyFire1== NULL )
		CDXError( Screen , "Could not allocate sprites object!" );

	if(LadyFire1->Create( Screen , "./Gfx/BLHADOKEN1.bmp" , 144 , 173 , 22 , CDXMEM_VIDTHENSYS )<0 )
		CDXError( Screen , "Could not load sprites from BLHADOKEN1.bmp" );


  LadyFire1->SetNumFrames(22);

	LadyFire1->SetColorKey( );

  LadyFire1->SetPos(0,0);

  LadyFire1->SetVel(16,0);

  LadyFire1->SetBoundRect(Bounds,BA_HIDE);

  LadyFire1->SetHidden(TRUE);

  LadyFire1->SetShadowOffset(100,35);

  Lady1->SetFire(LadyFire1,8);


  Lady1->SetName("Lady");
//  InitPreloader(++InitLoader);

/////////////////////////////////////////////////////////////////////////////////
//Face the other way will ya..


  Lady2 = new CFighter( );
	if( Lady2 == NULL )
		CDXError( Screen , "Could not allocate sprites object!" );

	if( Lady2->Create( Screen , "./Gfx/BL2.bmp" , 187 , 230 , 195 , CDXMEM_SYSTEMONLY )<0 )
		CDXError( Screen , "Could not load sprites from BL2.bmp" );

	Lady2->SetColorKey( );
  
  Lady2->SetFighterState(FS_STANCE);
  

  Data.iFrames=11;
  Data.iStartFrame=0;
  Data.iDelay=0;
  Lady2->SetFighterStateData(FS_STANCE,Data);

  Data.iFrames=16;
  Data.iStartFrame=11;
  Data.iDelay=0;
  Lady2->SetFighterStateData(FS_WALKF,Data);

  Data.iFrames=16;
  Data.iStartFrame=27;
  Lady2->SetFighterStateData(FS_WALKB,Data);
  
  Data.iFrames=8;
  Data.iStartFrame=43;
  Lady2->SetFighterStateData(FS_JUMPU,Data);

   
  Data.iFrames=8;
  Data.iStartFrame=51;
  Lady2->SetFighterStateData(FS_JUMPUF,Data);


  Data.iFrames=8;
  Data.iStartFrame=59;
  Lady2->SetFighterStateData(FS_JUMPUB,Data);


  Data.iFrames=11;
  Data.iStartFrame=67;
  Data.iDelay=0;
  Lady2->SetFighterStateData(FS_CROUCH,Data);


  Data.iFrames=2;
  Data.iStartFrame=78;
  Data.iDelay=0;
  Lady2->SetFighterStateData(FS_FALLING,Data);

  
  Data.iFrames=12;
  Data.iStartFrame=80;
  Data.iDelay=0;
  Lady2->SetFighterStateData(FS_PUNCH,Data);


  Data.iFrames=20;
  Data.iStartFrame=92;
  Data.iDelay=0;
  Lady2->SetFighterStateData(FS_PUNCHU,Data);

  Data.iFrames=14;
  Data.iStartFrame=112;
  Data.iDelay=0;
  Lady2->SetFighterStateData(FS_PUNCHUF,Data);


  Data.iFrames=22;
  Data.iStartFrame=126;
  Data.iDelay=0;
  Lady2->SetFighterStateData(FS_HADOKEN,Data);


  Data.iFrames=7;
  Data.iStartFrame=148;
  Data.iDelay=0;
  Lady2->SetFighterStateData(FS_PUNCHC,Data);


  Data.iFrames=14;
  Data.iStartFrame=155;
  Data.iDelay=0;
  Lady2->SetFighterStateData(FS_KICK,Data);


  Data.iFrames=13;
  Data.iStartFrame=169;
  Data.iDelay=0;
  Lady2->SetFighterStateData(FS_KICKU,Data);

  Data.iFrames=8;
  Data.iStartFrame=182;
  Data.iDelay=0;
  Lady2->SetFighterStateData(FS_KICKUF,Data);

  Data.iFrames=5;
  Data.iStartFrame=190;
  Data.iDelay=0;
  Lady2->SetFighterStateData(FS_KICKC,Data);






  Lady2->SetWalkVelocity(-10);
  JumpVelo.x= -14;
  JumpVelo.y=  24;
  Lady2->SetJumpVelocity(JumpVelo);


  Data.iDelay=0;


  SetRect(&CollRect,70,100,45,40);
  Lady2->SetCollisionRect(CollRect);

  Lady2->SetShadowOffset(100,35);
	

  Lady2->SetPos(350,503);

//  InitPreloader(InitLoader+=10);

/////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
  //What is a fighter without firepower??
  LadyFire2=new CSprite();
  
  if(LadyFire2== NULL )
		CDXError( Screen , "Could not allocate sprites object!" );

	if(LadyFire2->Create( Screen , "./Gfx/BLHADOKEN2.bmp" , 144 , 173 , 22 , CDXMEM_VIDTHENSYS )<0 )
		CDXError( Screen , "Could not load sprites from BLHADOKEN2.bmp" );


  LadyFire2->SetNumFrames(22);

	LadyFire2->SetColorKey( );

  LadyFire2->SetPos(0,0);

  LadyFire2->SetVel(-16,0);

  LadyFire2->SetBoundRect(Bounds,BA_HIDE);

  LadyFire2->SetHidden(TRUE);

  LadyFire2->SetShadowOffset(100,35);

  Lady2->SetFire(LadyFire2,8,90);

  Lady2->SetName("Lady");


//  InitPreloader(++InitLoader);


  
/////////////////////////////////////////////////////////////////////////////////
//Splash Screen
  
  LadySplash=g_LadySplash;


  Lady1->SetTotalFrames(195);
  Lady2->SetTotalFrames(195);



/////////////////////////////////////////////////////////////////////////////////
    //Okay now.... which fighter is it??
    if(PlayerNo==1)
    {    
        Lady=Lady1;

        CKEY_LEFTARROW=CDXKEY_LEFTARROW ;
        CKEY_RIGHTARROW=CDXKEY_RIGHTARROW;
/////////////////////////////////////////////////////////////////////////////////


        g_FirstFighter=Lady;
        g_FirstFighter1=Lady1;
        g_FirstFighter2=Lady2;

        g_FirstSplash=LadySplash;
    }
    else
    {
        Lady=Lady2;

       
/////////////////////////////////////////////////////////////////////////////////


        g_SecondFighter=Lady;
        g_SecondFighter1=Lady1;
        g_SecondFighter2=Lady2;

        g_SecondSplash=LadySplash;

        CKEY_LEFTARROW=CDXKEY_RIGHTARROW ;
        CKEY_RIGHTARROW=CDXKEY_LEFTARROW;

    }

}



void LoadBruce(int PlayerNo)
{

    
    POINT JumpVelo;
    RECT Bounds;
    STATEDATA Data;
    RECT CollRect;

    CSprite*BruceFire1;
    CSprite*BruceFire2;


    SetRect(&Bounds,0,0,1024,768);

    CFighter *Bruce,*Bruce1,*Bruce2;



/////////////////////////////////////////////////////////////
//Now it is time for the Bas to make its appearance in sys memory !!
/////////////////////////////////////////////////////////////////

  Bruce1 = new CFighter( );
	if( Bruce1 == NULL )
		CDXError( Screen , "Could not allocate sprites object!" );

	if( Bruce1->Create( Screen , "./Gfx/BAS1.bmp" , 232 , 178 , 153 , CDXMEM_SYSTEMONLY )<0 )
		CDXError( Screen , "Could not load sprites from BAS1.bmp" );

	Bruce1->SetColorKey( );
  
  Bruce1->SetFighterState(FS_STANCE);
  

  Data.iFrames=10;
  Data.iStartFrame=0;
  Data.iDelay=0;
  Bruce1->SetFighterStateData(FS_STANCE,Data);

  Data.iFrames=11;
  Data.iStartFrame=10;
  Data.iDelay=0;
  Bruce1->SetFighterStateData(FS_WALKF,Data);

  Data.iFrames=11;
  Data.iStartFrame=21;
  Bruce1->SetFighterStateData(FS_WALKB,Data);
  
  Data.iFrames=11;
  Data.iStartFrame=32;
  Bruce1->SetFighterStateData(FS_JUMPU,Data);

   
  Data.iFrames=13;
  Data.iStartFrame=43;
  Bruce1->SetFighterStateData(FS_JUMPUF,Data);


  Data.iFrames=13;
  Data.iStartFrame=56;
  Bruce1->SetFighterStateData(FS_JUMPUB,Data);


  Data.iFrames=5;
  Data.iStartFrame=69;
  Data.iDelay=0;
  Bruce1->SetFighterStateData(FS_CROUCH,Data);


  Data.iFrames=2;
  Data.iStartFrame=41;
  Data.iDelay=0;
  Bruce1->SetFighterStateData(FS_FALLING,Data);

  
  Data.iFrames=12;
  Data.iStartFrame=74;
  Data.iDelay=0;
  Bruce1->SetFighterStateData(FS_PUNCH,Data);


  Data.iFrames=9;
  Data.iStartFrame=86;
  Data.iDelay=0;
  Bruce1->SetFighterStateData(FS_PUNCHU,Data);

  Data.iFrames=8;
  Data.iStartFrame=95;
  Data.iDelay=0;
  Bruce1->SetFighterStateData(FS_PUNCHUF,Data);


  Data.iFrames=14;
  Data.iStartFrame=103;
  Data.iDelay=0;
  Bruce1->SetFighterStateData(FS_HADOKEN,Data);


  Data.iFrames=5;
  Data.iStartFrame=117;
  Data.iDelay=0;
  Bruce1->SetFighterStateData(FS_PUNCHC,Data);


  Data.iFrames=7;
  Data.iStartFrame=122;
  Data.iDelay=0;
  Bruce1->SetFighterStateData(FS_KICK,Data);


  Data.iFrames=6;
  Data.iStartFrame=129;
  Data.iDelay=0;
  Bruce1->SetFighterStateData(FS_KICKU,Data);

  Data.iFrames=8;
  Data.iStartFrame=135;
  Data.iDelay=0;
  Bruce1->SetFighterStateData(FS_KICKUF,Data);

  Data.iFrames=10;
  Data.iStartFrame=143;
  Data.iDelay=0;
  Bruce1->SetFighterStateData(FS_KICKC,Data);








  Data.iDelay=0;

  JumpVelo.x=12;
  JumpVelo.y=24;
  Bruce1->SetJumpVelocity(JumpVelo);



  SetRect(&CollRect,50,75,125,30);
  Bruce1->SetCollisionRect(CollRect);

  Bruce1->SetShadowOffset(100,35);
	

  Bruce1->SetPos(350,503);

//  InitPreloader(InitLoader+=10);

/////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
  
    //What is a fighter without firepower??
  BruceFire1=new CSprite();
  
  if(BruceFire1== NULL )
		CDXError( Screen , "Could not allocate sprites object!" );

	if(BruceFire1->Create( Screen , "./Gfx/BASHADOKEN1.bmp" , 64 , 135 , 15 , CDXMEM_VIDTHENSYS )<0 )
		CDXError( Screen , "Could not load sprites from BASHADOKEN1.bmp" );


  BruceFire1->SetNumFrames(15);

	BruceFire1->SetColorKey( );

  BruceFire1->SetPos(0,0);

  BruceFire1->SetVel(16,0);

  //BruceFire1->SetDelay(2);

  BruceFire1->SetBoundRect(Bounds,BA_HIDE);

  BruceFire1->SetHidden(TRUE);

  BruceFire1->SetShadowOffset(100,35);

  Bruce1->SetFire(BruceFire1,4,70);//,0);//70);

  Bruce1->SetName("Bruce");


//  InitPreloader(++InitLoader);

  

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
//Now it is time for the Bas to make its appearance in sys memory !!
/////////////////////////////////////////////////////////////////

  Bruce2 = new CFighter( );

	if( Bruce2 == NULL )
		CDXError( Screen , "Could not allocate sprites object!" );

	if( Bruce2->Create( Screen , "./Gfx/BAS2.bmp" , 232 , 178 , 153 , CDXMEM_SYSTEMONLY )<0 )
		CDXError( Screen , "Could not load sprites from BAS2.bmp" );

	Bruce2->SetColorKey( );
  
  Bruce2->SetFighterState(FS_STANCE);
  

  Data.iFrames=10;
  Data.iStartFrame=0;
  Data.iDelay=0;
  Bruce2->SetFighterStateData(FS_STANCE,Data);

  Data.iFrames=11;
  Data.iStartFrame=10;
  Data.iDelay=0;
  Bruce2->SetFighterStateData(FS_WALKF,Data);

  Data.iFrames=11;
  Data.iStartFrame=21;
  Bruce2->SetFighterStateData(FS_WALKB,Data);
  
  Data.iFrames=11;
  Data.iStartFrame=32;
  Bruce2->SetFighterStateData(FS_JUMPU,Data);

   
  Data.iFrames=13;
  Data.iStartFrame=43;
  Bruce2->SetFighterStateData(FS_JUMPUF,Data);


  Data.iFrames=13;
  Data.iStartFrame=56;
  Bruce2->SetFighterStateData(FS_JUMPUB,Data);


  Data.iFrames=5;
  Data.iStartFrame=69;
  Data.iDelay=0;
  Bruce2->SetFighterStateData(FS_CROUCH,Data);


  Data.iFrames=2;
  Data.iStartFrame=41;
  Data.iDelay=0;
  Bruce2->SetFighterStateData(FS_FALLING,Data);

  
  Data.iFrames=12;
  Data.iStartFrame=74;
  Data.iDelay=0;
  Bruce2->SetFighterStateData(FS_PUNCH,Data);


  Data.iFrames=9;
  Data.iStartFrame=86;
  Data.iDelay=0;
  Bruce2->SetFighterStateData(FS_PUNCHU,Data);

  Data.iFrames=8;
  Data.iStartFrame=95;
  Data.iDelay=0;
  Bruce2->SetFighterStateData(FS_PUNCHUF,Data);


  Data.iFrames=14;
  Data.iStartFrame=103;
  Data.iDelay=0;
  Bruce2->SetFighterStateData(FS_HADOKEN,Data);


  Data.iFrames=5;
  Data.iStartFrame=117;
  Data.iDelay=0;
  Bruce2->SetFighterStateData(FS_PUNCHC,Data);


  Data.iFrames=7;
  Data.iStartFrame=122;
  Data.iDelay=0;
  Bruce2->SetFighterStateData(FS_KICK,Data);


  Data.iFrames=6;
  Data.iStartFrame=129;
  Data.iDelay=0;
  Bruce2->SetFighterStateData(FS_KICKU,Data);

  Data.iFrames=8;
  Data.iStartFrame=135;
  Data.iDelay=0;
  Bruce2->SetFighterStateData(FS_KICKUF,Data);

  Data.iFrames=10;
  Data.iStartFrame=143;
  Data.iDelay=0;
  Bruce2->SetFighterStateData(FS_KICKC,Data);




  Bruce2->SetWalkVelocity(-8);
  JumpVelo.x=-12;
  JumpVelo.y=24;
  Bruce2->SetJumpVelocity(JumpVelo);






  Data.iDelay=0;


  SetRect(&CollRect,125,75,50,30);
  Bruce2->SetCollisionRect(CollRect);

  Bruce2->SetShadowOffset(100,35);
	

  Bruce2->SetPos(400,503);

//  InitPreloader(InitLoader+=10);


/////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
  //What is a fighter without firepower??
  BruceFire2=new CSprite();
  
  if(BruceFire2== NULL )
		CDXError( Screen , "Could not allocate sprites object!" );

	if(BruceFire2->Create( Screen , "./Gfx/BASHADOKEN2.bmp" , 64 , 135 , 15 , CDXMEM_VIDTHENSYS )<0 )
		CDXError( Screen , "Could not load sprites from BASHADOKEN2.bmp" );


  BruceFire2->SetNumFrames(15);

	BruceFire2->SetColorKey( );

  BruceFire2->SetPos(0,0);

  BruceFire2->SetVel(-16,0);

  BruceFire2->SetBoundRect(Bounds,BA_HIDE);

  BruceFire2->SetHidden(TRUE);

  BruceFire2->SetShadowOffset(100,35);

  Bruce2->SetFire(BruceFire2,4,70);

  Bruce2->SetName("Bruce");


//  InitPreloader(++InitLoader);

/////////////////////////////////////////////////////////////////////////////////

//Splash Screen
    

    Bruce1->SetTotalFrames(153);
    Bruce2->SetTotalFrames(153);



/////////////////////////////////////////////////////////////////////////////////
//Okay now.... which fighter is it??
if(PlayerNo==1)
{
    Bruce=Bruce1;


    g_FirstFighter=Bruce;
    g_FirstFighter1=Bruce1;
    g_FirstFighter2=Bruce2;
    g_FirstSplash=g_BruceSplash;

}

else
{
    Bruce=Bruce2;
    
   // int SwapInput;
   // SwapInput=CKEY_BASLEFT;
   // CKEY_BASLEFT=CKEY_BASRIGHT;
   // CKEY_BASRIGHT=SwapInput;
/////////////////////////////////////////////////////////////////////////////////

    g_SecondFighter=Bruce;
    g_SecondFighter1=Bruce1;
    g_SecondFighter2=Bruce2;
    g_SecondSplash=g_BruceSplash;
}
    
}





void LoadCyrus(int PlayerNo)
{

    
    POINT JumpVelo;
    RECT Bounds;
    STATEDATA Data;
    RECT CollRect;

    CSprite*CyrusFire1;
    CSprite*CyrusFire2;


    SetRect(&Bounds,0,0,1024,768);

    CFighter *Cyrus,*Cyrus1,*Cyrus2;



/////////////////////////////////////////////////////////////
//Now it is time for the Bas to make its appearance in sys memory !!
/////////////////////////////////////////////////////////////////

  Cyrus1 = new CFighter( );
	if( Cyrus1 == NULL )
		CDXError( Screen , "Could not allocate sprites object!" );

	if( Cyrus1->Create( Screen , "./Gfx/CYRUS1.bmp" , 180 , 178 , 153 , CDXMEM_SYSTEMONLY )<0 )
		CDXError( Screen , "Could not load sprites from CYRUS1.bmp" );

	Cyrus1->SetColorKey( );
  
  Cyrus1->SetFighterState(FS_STANCE);
  

  Data.iFrames=10;
  Data.iStartFrame=0;
  Data.iDelay=0;
  Cyrus1->SetFighterStateData(FS_STANCE,Data);

  Data.iFrames=11;
  Data.iStartFrame=10;
  Data.iDelay=0;
  Cyrus1->SetFighterStateData(FS_WALKF,Data);

  Data.iFrames=11;
  Data.iStartFrame=21;
  Cyrus1->SetFighterStateData(FS_WALKB,Data);
  
  Data.iFrames=11;
  Data.iStartFrame=32;
  Cyrus1->SetFighterStateData(FS_JUMPU,Data);

   
  Data.iFrames=13;
  Data.iStartFrame=43;
  Cyrus1->SetFighterStateData(FS_JUMPUF,Data);

  //Decrease 1 Frame ...

  Data.iFrames=13;
  Data.iStartFrame=55;
  Cyrus1->SetFighterStateData(FS_JUMPUB,Data);


  Data.iFrames=5;
  Data.iStartFrame=68;
  Data.iDelay=0;
  Cyrus1->SetFighterStateData(FS_CROUCH,Data);

////////
  Data.iFrames=2;
  Data.iStartFrame=41;
  Data.iDelay=0;
  Cyrus1->SetFighterStateData(FS_FALLING,Data);
//////////
  


  Data.iFrames=12;
  Data.iStartFrame=73;
  Data.iDelay=0;
  Cyrus1->SetFighterStateData(FS_PUNCH,Data);



  Data.iFrames=9;
  Data.iStartFrame=85;
  Data.iDelay=0;
  Cyrus1->SetFighterStateData(FS_PUNCHU,Data);


  Data.iFrames=8;
  Data.iStartFrame=94;
  Data.iDelay=0;
  Cyrus1->SetFighterStateData(FS_PUNCHUF,Data);


  Data.iFrames=13;
  Data.iStartFrame=102;
  Data.iDelay=0;
  Cyrus1->SetFighterStateData(FS_HADOKEN,Data);


  //-1 From Start Frame count

  Data.iFrames=5;
  Data.iStartFrame=115;
  Data.iDelay=0;
  Cyrus1->SetFighterStateData(FS_PUNCHC,Data);


  Data.iFrames=8;
  Data.iStartFrame=120;
  Data.iDelay=0;
  Cyrus1->SetFighterStateData(FS_KICK,Data);

  //+1 to frame count

  Data.iFrames=6;
  Data.iStartFrame=128;
  Data.iDelay=0;
  Cyrus1->SetFighterStateData(FS_KICKU,Data);

  Data.iFrames=8;
  Data.iStartFrame=134;
  Data.iDelay=0;
  Cyrus1->SetFighterStateData(FS_KICKUF,Data);

  Data.iFrames=10;
  Data.iStartFrame=142;
  Data.iDelay=0;
  Cyrus1->SetFighterStateData(FS_KICKC,Data);








  Data.iDelay=0;

  JumpVelo.x=12;
  JumpVelo.y=24;
  Cyrus1->SetJumpVelocity(JumpVelo);



  SetRect(&CollRect,30,75,75,20);
  Cyrus1->SetCollisionRect(CollRect);

  Cyrus1->SetShadowOffset(100,35);
	

  Cyrus1->SetPos(350,503);

//  InitPreloader(InitLoader+=10);

/////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
  
    //What is a fighter without firepower??
  CyrusFire1=new CSprite();
  
  if(CyrusFire1== NULL )
		CDXError( Screen , "Could not allocate sprites object!" );

	if(CyrusFire1->Create( Screen , "./Gfx/CYRUSFIRE1.bmp" , 124 , 90 , 16 , CDXMEM_VIDTHENSYS )<0 )
		CDXError( Screen , "Could not load sprites from CYRUSFIRE1.bmp" );


  CyrusFire1->SetNumFrames(16);

	CyrusFire1->SetColorKey( );

  CyrusFire1->SetPos(0,0);

  CyrusFire1->SetVel(11,0);

  //CyrusFire1->SetDelay(2);

  CyrusFire1->SetBoundRect(Bounds,BA_HIDE);

  CyrusFire1->SetHidden(TRUE);

  CyrusFire1->SetShadowOffset(100,35);

  Cyrus1->SetFire(CyrusFire1,4,70,50);//,0);//70);

  Cyrus1->SetName("Cyrus");

//  InitPreloader(++InitLoader);

  

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
//Now it is time for the Bas to make its appearance in sys memory !!
/////////////////////////////////////////////////////////////////

  Cyrus2 = new CFighter( );

	if( Cyrus2 == NULL )
		CDXError( Screen , "Could not allocate sprites object!" );

	if( Cyrus2->Create( Screen , "./Gfx/CYRUS2.bmp" , 180 , 178 , 153 , CDXMEM_SYSTEMONLY )<0 )
		CDXError( Screen , "Could not load sprites from CYRUS2.bmp" );

	Cyrus2->SetColorKey( );
  
  Cyrus2->SetFighterState(FS_STANCE);
  

  Data.iFrames=10;
  Data.iStartFrame=0;
  Data.iDelay=0;
  Cyrus2->SetFighterStateData(FS_STANCE,Data);

  Data.iFrames=11;
  Data.iStartFrame=10;
  Data.iDelay=0;
  Cyrus2->SetFighterStateData(FS_WALKF,Data);

  Data.iFrames=11;
  Data.iStartFrame=21;
  Cyrus2->SetFighterStateData(FS_WALKB,Data);
  
  Data.iFrames=11;
  Data.iStartFrame=32;
  Cyrus2->SetFighterStateData(FS_JUMPU,Data);

   
  Data.iFrames=13;
  Data.iStartFrame=43;
  Cyrus2->SetFighterStateData(FS_JUMPUF,Data);

  //Decrease 1 Frame ...

  Data.iFrames=13;
  Data.iStartFrame=55;
  Cyrus2->SetFighterStateData(FS_JUMPUB,Data);


  Data.iFrames=5;
  Data.iStartFrame=68;
  Data.iDelay=0;
  Cyrus2->SetFighterStateData(FS_CROUCH,Data);

////////
  Data.iFrames=2;
  Data.iStartFrame=41;
  Data.iDelay=0;
  Cyrus2->SetFighterStateData(FS_FALLING,Data);
//////////
  


  Data.iFrames=12;
  Data.iStartFrame=73;
  Data.iDelay=0;
  Cyrus2->SetFighterStateData(FS_PUNCH,Data);



  Data.iFrames=9;
  Data.iStartFrame=85;
  Data.iDelay=0;
  Cyrus2->SetFighterStateData(FS_PUNCHU,Data);


  Data.iFrames=8;
  Data.iStartFrame=94;
  Data.iDelay=0;
  Cyrus2->SetFighterStateData(FS_PUNCHUF,Data);


  Data.iFrames=13;
  Data.iStartFrame=102;
  Data.iDelay=0;
  Cyrus2->SetFighterStateData(FS_HADOKEN,Data);


  //-1 From Start Frame count

  Data.iFrames=5;
  Data.iStartFrame=115;
  Data.iDelay=0;
  Cyrus2->SetFighterStateData(FS_PUNCHC,Data);


  Data.iFrames=8;
  Data.iStartFrame=120;
  Data.iDelay=0;
  Cyrus2->SetFighterStateData(FS_KICK,Data);

  //+1 to frame count

  Data.iFrames=6;
  Data.iStartFrame=128;
  Data.iDelay=0;
  Cyrus2->SetFighterStateData(FS_KICKU,Data);

  Data.iFrames=8;
  Data.iStartFrame=134;
  Data.iDelay=0;
  Cyrus2->SetFighterStateData(FS_KICKUF,Data);

  Data.iFrames=10;
  Data.iStartFrame=142;
  Data.iDelay=0;
  Cyrus2->SetFighterStateData(FS_KICKC,Data);




  Cyrus2->SetWalkVelocity(-8);
  JumpVelo.x=-12;
  JumpVelo.y=24;
  Cyrus2->SetJumpVelocity(JumpVelo);






  Data.iDelay=0;


  SetRect(&CollRect,75,75,30,20);
  Cyrus2->SetCollisionRect(CollRect);

  Cyrus2->SetShadowOffset(100,35);
	

  Cyrus2->SetPos(400,503);

//  InitPreloader(InitLoader+=10);


/////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
  //What is a fighter without firepower??
  CyrusFire2=new CSprite();
  
  if(CyrusFire2== NULL )
		CDXError( Screen , "Could not allocate sprites object!" );

	if(CyrusFire2->Create( Screen , "./Gfx/CYRUSFIRE2.bmp" , 124 , 90 , 16 , CDXMEM_VIDTHENSYS )<0 )
		CDXError( Screen , "Could not load sprites from CYRUSFIRE2.bmp" );


  CyrusFire2->SetNumFrames(16);

	CyrusFire2->SetColorKey( );

  CyrusFire2->SetPos(0,0);

  CyrusFire2->SetVel(-11,0);

  CyrusFire2->SetBoundRect(Bounds,BA_HIDE);

  CyrusFire2->SetHidden(TRUE);

  CyrusFire2->SetShadowOffset(100,35);

  Cyrus2->SetFire(CyrusFire2,4,70,50);

  Cyrus2->SetName("Cyrus");

//  InitPreloader(++InitLoader);

/////////////////////////////////////////////////////////////////////////////////

//Splash Screen
    Cyrus1->SetTotalFrames(152);
    Cyrus2->SetTotalFrames(152);



/////////////////////////////////////////////////////////////////////////////////
//Okay now.... which fighter is it??
if(PlayerNo==1)
{
    Cyrus=Cyrus1;


    g_FirstFighter=Cyrus;
    g_FirstFighter1=Cyrus1;
    g_FirstFighter2=Cyrus2;
    g_FirstSplash=g_CyrusSplash;

}

else
{
    Cyrus=Cyrus2;
    
   // int SwapInput;
   // SwapInput=CKEY_BASLEFT;
   // CKEY_BASLEFT=CKEY_BASRIGHT;
   // CKEY_BASRIGHT=SwapInput;
/////////////////////////////////////////////////////////////////////////////////

    g_SecondFighter=Cyrus;
    g_SecondFighter1=Cyrus1;
    g_SecondFighter2=Cyrus2;
    g_SecondSplash=g_CyrusSplash;
}
    
}



void LoadJunior(int PlayerNo)
{

    
    POINT JumpVelo;
    RECT Bounds;
    STATEDATA Data;
    RECT CollRect;

    CSprite*JuniorFire1;
    CSprite*JuniorFire2;


    SetRect(&Bounds,0,0,1024,768);

    CFighter *Junior,*Junior1,*Junior2;



/////////////////////////////////////////////////////////////
//Now it is time for the Bas to make its appearance in sys memory !!
/////////////////////////////////////////////////////////////////

  Junior1 = new CFighter( );
	if( Junior1 == NULL )
		CDXError( Screen , "Could not allocate sprites object!" );

	if( Junior1->Create( Screen , "./Gfx/Junior1.bmp" , 168  , 170 , 153 , CDXMEM_SYSTEMONLY )<0 )
		CDXError( Screen , "Could not load sprites from Junior1.bmp" );

	Junior1->SetColorKey( );
  
  Junior1->SetFighterState(FS_STANCE);
  

  Data.iFrames=10;
  Data.iStartFrame=0;
  Data.iDelay=0;
  Junior1->SetFighterStateData(FS_STANCE,Data);

  
  Data.iFrames=11;
  Data.iStartFrame=10;
  Data.iDelay=0;
  Junior1->SetFighterStateData(FS_WALKF,Data);

  Data.iFrames=11;
  Data.iStartFrame=21;
  Junior1->SetFighterStateData(FS_WALKB,Data);
  
  Data.iFrames=11;
  Data.iStartFrame=32;
  Junior1->SetFighterStateData(FS_JUMPU,Data);

   
  Data.iFrames=13;
  Data.iStartFrame=43;
  Junior1->SetFighterStateData(FS_JUMPUF,Data);

  

  Data.iFrames=13;
  Data.iStartFrame=55;
  Junior1->SetFighterStateData(FS_JUMPUB,Data);


  Data.iFrames=5;
  Data.iStartFrame=68;
  Data.iDelay=0;
  Junior1->SetFighterStateData(FS_CROUCH,Data);

////////
  Data.iFrames=2;
  Data.iStartFrame=41;
  Data.iDelay=0;
  Junior1->SetFighterStateData(FS_FALLING,Data);
//////////
  


  Data.iFrames=12;
  Data.iStartFrame=73;
  Data.iDelay=0;
  Junior1->SetFighterStateData(FS_PUNCH,Data);



  Data.iFrames=9;
  Data.iStartFrame=85;
  Data.iDelay=0;
  Junior1->SetFighterStateData(FS_PUNCHU,Data);


  Data.iFrames=8;
  Data.iStartFrame=94;
  Data.iDelay=0;
  Junior1->SetFighterStateData(FS_PUNCHUF,Data);


  Data.iFrames=13;
  Data.iStartFrame=102;
  Data.iDelay=0;
  Junior1->SetFighterStateData(FS_HADOKEN,Data);


  

  Data.iFrames=5;
  Data.iStartFrame=115;
  Data.iDelay=0;
  Junior1->SetFighterStateData(FS_PUNCHC,Data);



  Data.iFrames=8;
  Data.iStartFrame=120;
  Data.iDelay=0;
  Junior1->SetFighterStateData(FS_KICK,Data);

  

  Data.iFrames=6;
  Data.iStartFrame=128;
  Data.iDelay=0;
  Junior1->SetFighterStateData(FS_KICKU,Data);

  Data.iFrames=8;
  Data.iStartFrame=134;
  Data.iDelay=0;
  Junior1->SetFighterStateData(FS_KICKUF,Data);

  Data.iFrames=10;
  Data.iStartFrame=142;
  Data.iDelay=0;
  Junior1->SetFighterStateData(FS_KICKC,Data);








  Data.iDelay=0;

  JumpVelo.x=12;
  JumpVelo.y=24;
  Junior1->SetJumpVelocity(JumpVelo);



  SetRect(&CollRect,30,75,75,20);
  Junior1->SetCollisionRect(CollRect);

  Junior1->SetShadowOffset(100,35);
	

  Junior1->SetPos(350,503);

//  InitPreloader(InitLoader+=10);

/////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
  
    //What is a fighter without firepower??
  JuniorFire1=new CSprite();
  
  if(JuniorFire1== NULL )
		CDXError( Screen , "Could not allocate sprites object!" );

	if(JuniorFire1->Create( Screen , "./Gfx/JuniorHadoken.bmp" , 23 , 18 , 15 , CDXMEM_VIDTHENSYS )<0 )
		CDXError( Screen , "Could not load sprites from JuniorHadoken.bmp" );


  JuniorFire1->SetNumFrames(8);

	JuniorFire1->SetColorKey( );

  JuniorFire1->SetPos(0,0);

  JuniorFire1->SetVel(16,0);

  //JuniorFire1->SetDelay(2);

  JuniorFire1->SetBoundRect(Bounds,BA_HIDE);

  JuniorFire1->SetHidden(TRUE);

  JuniorFire1->SetShadowOffset(100,35);

  Junior1->SetFire(JuniorFire1,4,85,80);//,0);//70);


//  InitPreloader(++InitLoader);

  
 Junior1->SetName("Junior");

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
//Now it is time for the Bas to make its appearance in sys memory !!
/////////////////////////////////////////////////////////////////

  Junior2 = new CFighter( );

	if( Junior2 == NULL )
		CDXError( Screen , "Could not allocate sprites object!" );

	if( Junior2->Create( Screen , "./Gfx/Junior2.bmp" , 168 , 170 , 153 , CDXMEM_SYSTEMONLY )<0 )
		CDXError( Screen , "Could not load sprites from Junior2.bmp" );

	Junior2->SetColorKey( );
  
  Junior2->SetFighterState(FS_STANCE);
  

  Data.iFrames=10;
  Data.iStartFrame=0;
  Data.iDelay=0;
  Junior2->SetFighterStateData(FS_STANCE,Data);

  Data.iFrames=11;
  Data.iStartFrame=10;
  Data.iDelay=0;
  Junior2->SetFighterStateData(FS_WALKF,Data);

  Data.iFrames=11;
  Data.iStartFrame=21;
  Junior2->SetFighterStateData(FS_WALKB,Data);
  
  Data.iFrames=11;
  Data.iStartFrame=32;
  Junior2->SetFighterStateData(FS_JUMPU,Data);

   
  Data.iFrames=13;
  Data.iStartFrame=43;
  Junior2->SetFighterStateData(FS_JUMPUF,Data);

  

  Data.iFrames=13;
  Data.iStartFrame=55;
  Junior2->SetFighterStateData(FS_JUMPUB,Data);


  Data.iFrames=5;
  Data.iStartFrame=68;
  Data.iDelay=0;
  Junior2->SetFighterStateData(FS_CROUCH,Data);


  Data.iFrames=2;
  Data.iStartFrame=41;
  Data.iDelay=0;
  Junior2->SetFighterStateData(FS_FALLING,Data);

  


  Data.iFrames=12;
  Data.iStartFrame=73;
  Data.iDelay=0;
  Junior2->SetFighterStateData(FS_PUNCH,Data);



  Data.iFrames=9;
  Data.iStartFrame=85;
  Data.iDelay=0;
  Junior2->SetFighterStateData(FS_PUNCHU,Data);


  Data.iFrames=8;
  Data.iStartFrame=94;
  Data.iDelay=0;
  Junior2->SetFighterStateData(FS_PUNCHUF,Data);


  Data.iFrames=13;
  Data.iStartFrame=102;
  Data.iDelay=0;
  Junior2->SetFighterStateData(FS_HADOKEN,Data);


  //-1 From Start Frame count

  Data.iFrames=5;
  Data.iStartFrame=115;
  Data.iDelay=0;
  Junior2->SetFighterStateData(FS_PUNCHC,Data);


  Data.iFrames=8;
  Data.iStartFrame=120;
  Data.iDelay=0;
  Junior2->SetFighterStateData(FS_KICK,Data);

  //+1 to frame count

  Data.iFrames=6;
  Data.iStartFrame=128;
  Data.iDelay=0;
  Junior2->SetFighterStateData(FS_KICKU,Data);

  Data.iFrames=8;
  Data.iStartFrame=134;
  Data.iDelay=0;
  Junior2->SetFighterStateData(FS_KICKUF,Data);

  Data.iFrames=10;
  Data.iStartFrame=142;
  Data.iDelay=0;
  Junior2->SetFighterStateData(FS_KICKC,Data);




  Junior2->SetWalkVelocity(-8);
  JumpVelo.x=-12;
  JumpVelo.y=24;
  Junior2->SetJumpVelocity(JumpVelo);






  Data.iDelay=0;


  SetRect(&CollRect,75,75,30,20);
  Junior2->SetCollisionRect(CollRect);

  Junior2->SetShadowOffset(100,35);
	

  Junior2->SetPos(400,503);

//  InitPreloader(InitLoader+=10);


/////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
  //What is a fighter without firepower??
  JuniorFire2=new CSprite();
  
  if(JuniorFire2== NULL )
		CDXError( Screen , "Could not allocate sprites object!" );

	if(JuniorFire2->Create( Screen , "./Gfx/JuniorHadoken.bmp" , 23 , 18 , 15 , CDXMEM_VIDTHENSYS )<0 )
		CDXError( Screen , "Could not load sprites from juniorhadoken.bmp" );


  JuniorFire2->SetNumFrames(8);

	JuniorFire2->SetColorKey( );

  JuniorFire2->SetPos(0,0);

  JuniorFire2->SetVel(-16,0);

  JuniorFire2->SetBoundRect(Bounds,BA_HIDE);

  JuniorFire2->SetHidden(TRUE);

  JuniorFire2->SetShadowOffset(100,35);

  Junior2->SetFire(JuniorFire2,4,85,80);

  Junior2->SetName("Junior");


//  InitPreloader(++InitLoader);

/////////////////////////////////////////////////////////////////////////////////

//Splash Screen

    Junior1->SetTotalFrames(152);
    Junior2->SetTotalFrames(152);



/////////////////////////////////////////////////////////////////////////////////
//Okay now.... which fighter is it??
if(PlayerNo==1)
{
    Junior=Junior1;


    g_FirstFighter=Junior;
    g_FirstFighter1=Junior1;
    g_FirstFighter2=Junior2;
    g_FirstSplash=g_JuniorSplash;

}

else
{
    Junior=Junior2;
    
   // int SwapInput;
   // SwapInput=CKEY_BASLEFT;
   // CKEY_BASLEFT=CKEY_BASRIGHT;
   // CKEY_BASRIGHT=SwapInput;
/////////////////////////////////////////////////////////////////////////////////

    g_SecondFighter=Junior;
    g_SecondFighter1=Junior1;
    g_SecondFighter2=Junior2;
    g_SecondSplash=g_JuniorSplash;
}
    
}





