//-----------------------------------------------------------------
// Background Object
// C++ Header - Background.h
//-----------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------
#include <windows.h>
#define CDXINCLUDEALL     // this define includes all headers, otherwise include one by one
#include <cdx.h>



//-----------------------------------------------------------------
// Starry Background Class
//-----------------------------------------------------------------
class StarryBackground 
{
protected:
  // Member Variables
  CDXScreen * m_Screen;
  int       m_iNumStars;
  int       m_iTwinkleDelay;
  POINT     m_ptStars[1000];
  COLORREF  m_crStarColors[1000];

public:
  // Constructor(s)/Destructor
          StarryBackground(CDXScreen * Screen, int iNumStars = 100,
            int iTwinkleDelay = 100);
  virtual ~StarryBackground();

  // General Methods
   void  Update();
   void  Draw();
};
