//-----------------------------------------------------------------
// Background Object
// C++ Source - Background.cpp
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------
#include "TileAnimator.h"


//-----------------------------------------------------------------
// StarryBackground Constructor
//-----------------------------------------------------------------
TileAnimator::TileAnimator(CDXMap *Map, int iNumTiles,
  int iFrameDelay)  
{


  // Initialize the member variables
  m_Map=Map;
  m_iNumTiles = min(iNumTiles, 50);
  
  m_iFrameDelay = iFrameDelay;

  m_iCurrentTile= 0;

  // Create the stars
  /*
  for (int i = 0; i < iNumTiles; i++)
  {
    m_ptStars[i].x = rand() % m_Screen->GetWidth();
    m_ptStars[i].y = rand() % m_Screen->GetHeight();
    m_crStarColors[i] = RGB(128, 128, 128);
  }
  */
}

TileAnimator::~TileAnimator()
{
}

//-----------------------------------------------------------------
// StarryBackground General Methods
//-----------------------------------------------------------------
void TileAnimator::Update()
{
  // Randomly change the shade of the stars so that they twinkle
 static int FrameTrigger=m_iFrameDelay;
 
 if(--FrameTrigger<=0)
 {
     FrameTrigger=m_iFrameDelay;
     m_Map->SetTile(m_ptTile.x,m_ptTile.y,m_TileValues[m_iCurrentTile]); 
     m_iCurrentTile++;
     if(m_iCurrentTile==m_iNumTiles)
         m_iCurrentTile=0;
 } 

}



void TileAnimator::SetTiles(int a[],POINT point)
{
    for(int i=0;i<m_iNumTiles;i++)
        m_TileValues[i]=a[i];
    m_ptTile=point;

}

void TileAnimator::SetMap(CDXMap * Map)
{
    m_Map=Map;
}