//-----------------------------------------------------------------
// Background Object
// C++ Source - Background.cpp
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------
#include "StarryBackground.h"


//-----------------------------------------------------------------
// StarryBackground Constructor
//-----------------------------------------------------------------
StarryBackground::StarryBackground(CDXScreen *Screen, int iNumStars,
  int iTwinkleDelay)  
{


  // Initialize the member variables
  m_Screen=Screen;
  m_iNumStars = min(iNumStars, 1000);
  m_iTwinkleDelay = iTwinkleDelay;

  // Create the stars
  for (int i = 0; i < iNumStars; i++)
  {
    m_ptStars[i].x = rand() % m_Screen->GetWidth();
    m_ptStars[i].y = rand() % m_Screen->GetHeight();
    m_crStarColors[i] = RGB(128, 128, 128);
  }
}

StarryBackground::~StarryBackground()
{
}

//-----------------------------------------------------------------
// StarryBackground General Methods
//-----------------------------------------------------------------
void StarryBackground::Update()
{
  // Randomly change the shade of the stars so that they twinkle
  int iRGB;
  for (int i = 0; i < m_iNumStars; i++)
    if ((rand() % m_iTwinkleDelay) == 0)
    {
      iRGB = rand() % 256;
      m_crStarColors[i] = RGB(iRGB, iRGB, iRGB);
    }
}

void StarryBackground::Draw()
{
  // Draw the solid black background
  
  Update();
    
  m_Screen->GetBack()->Lock();
  // Draw the stars
  for (int i = 0; i < m_iNumStars; i++)
  {
  
    m_Screen->GetBack()->PutPixel(m_ptStars[i].x, m_ptStars[i].y, m_crStarColors[i]);
  }

  m_Screen->GetBack()->UnLock();
  
}
