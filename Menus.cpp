#include "FighterX.h"
#include "CDXMenu.h"
#include "CDXHorizTextScroller.h"
#include "CDXVertTextScroller.h"


CDXBitmapFont* BigFont;
CDXBitmapFont* BiggerFont;

CDXVertTextScroller * CreditsScroll;
CDXBitmapFont * CreditsFont;
//RECT CreditsClipRect = { 0 , 0 , 500 , 768 };    
RECT CreditsClipRect = { 0 , 0 , 390 , 768 };    


CDXMenu* MainMenu;
extern CDXSprite *PlayerMenuLogo;
CDXSprite * MainMenuLogo;

#define MENU_ITEM_HEIGHT 40



CDXMenu* HelpMenu;


extern CDXScreen* Screen;
extern CDXInput* Input;


extern int g_ProgramState;
extern HWND g_hWnd;

extern int g_Winner;


void cdx_InitMenus()
{
    MainMenu=new CDXMenu();
    MainMenu->Create(Screen->GetBack(), MENU_ITEM_HEIGHT);
    //MainMenu->SetTitle("Main Menu", RGB(255, 255, 0));

    Screen->GetBack()->ChangeFont("Verdana", 0, MENU_ITEM_HEIGHT, FW_BOLD);


	  // Add some options to the menu
    MainMenu->AddItem("New Game", RGB(0, 0, 255), RGB(255,255, 255));
    MainMenu->AddItem("Select Map",RGB(0,0,255),RGB(255,255,255));
	  MainMenu->AddItem("Help", RGB(0, 0, 255), RGB(255,255, 255));
	  MainMenu->AddItem("Credits", RGB(0, 0, 255), RGB(255,255, 255));
	  MainMenu->AddItem("Quit", RGB(0, 0, 255), RGB(255,255, 255));

    MainMenuLogo=new CDXSprite;
    MainMenuLogo->Create(Screen, "./Gfx/Menu.jpg", 1024, 768 , 1 , CDXMEM_SYSTEMONLY);
    //Screen->GetBack()->Fill(RGB(255,255,255));
    MainMenuLogo->SetPos(0,0);


    CreditsFont = new CDXBitmapFont();
  	CreditsFont->Create( Screen , "Arial" , 22 , RGB( 0 , 0 , 190 ),0,0,FW_BOLD );

   
  CreditsScroll = new CDXVertTextScroller( Screen , &CreditsClipRect ,"-- C R E D I T S --\n\n\n\n\
DEVELOPMENT TEAM:\n\n\
PROGRAMMING BY BaSaRaT\n\
DESIGN BY BaSaRaT\n\n\n\
SPECIAL THANKS TO:\n\n\
Mom ... The Great.\n\n\
Ali Masud ... Prime Beta Tester.\n\n\
Zwiefus For the Graphics\nhttp://www.newwavemugen.com/~zweifuss\n\n\
The CDX Community.They made my life a lot easier.\nwww.cdxlib.com\n\n\
People i know.And you ..for Playing the Game.\n\n\n\n\
\nDownload the Game And Source at www.basarat.tk\
",CDX_CENTERJUSTIFY , 4 , CreditsFont );



    cdx_InitPlayerMenu();



    
}

void cdx_DoMainMenu()
{
    if(g_ProgramState==PS_CREDITS)
    {
        cdx_DoCredits();
        return;
    }
    

    cdx_HandleMainMenuKeys();

    Screen->GetBack()->ChangeFont("Verdana", 0, MENU_ITEM_HEIGHT, FW_BOLD);
    Screen->GetBack()->SetFont();
    MainMenuLogo->Draw(Screen->GetBack( ), 0,	0, CDXBLT_BLK);



    BigFont->SetColor(RGB(255,0,0));
    BiggerFont->SetColor(RGB(255,0,0));
    
    BigFont->DrawTrans(25,100,"Fighter",Screen->GetBack());
    BiggerFont->DrawTrans(50,600,"X",Screen->GetBack());

    BigFont->SetColor(RGB(255,255,255));


    MainMenu->Draw(100,300);
}

void cdx_HandleMainMenuKeys()
{
    //MainMenu->Home();
    if(Input->GetKeyState(CDXKEY_UPARROW)==CDXKEY_PRESS)
    {
        MainMenu->Up();
    }
    
    if(Input->GetKeyState(CDXKEY_DOWNARROW)==CDXKEY_PRESS)
    {
        MainMenu->Down();
    }

    if(Input->GetKeyState(CDXKEY_ENTER)==CDXKEY_PRESS)
    {
        switch(MainMenu->Enter())
        {
        case 0:
            //new game logic
            //cdx_RestartGame();
            g_ProgramState=PS_PLAYERONEMENU;
            break;
        case 1:
            g_ProgramState=PS_MAPMENU;
            cdx_InitMapMenu();
            break;
		case 2:
			g_ProgramState=PS_HELPMENU;
			cdx_InitHelpMenu();
			break;

        case 3:
            g_ProgramState=PS_CREDITS;
            CreditsScroll->ResetPosition();
            break;
        case 4:
            g_ProgramState=PS_QUIT;
 
            break;

        }
    }


}

void cdx_DeInitMenus()
{
  // SAFEDELETE(BigFont);
    cdx_DeInitPlayerMenu();


    SAFEDELETE(MainMenu);
    SAFEDELETE(MainMenuLogo);
}

void cdx_GameEnd()
{
    BigFont->DrawTrans(190,200,"GAME OVER",Screen->GetBack());
    if(g_Winner==1)
        BigFont->DrawTrans(190,300,"PLAYER ONE WINS!!",Screen->GetBack());
    else
        BigFont->DrawTrans(190,300,"PLAYER TWO WINS!!",Screen->GetBack());


}


void cdx_DoCredits()
{
	//Screen->GetBack()->Fill(0);

	
	MainMenuLogo->Draw(Screen->GetBack( ), 0,	0, CDXBLT_BLK);
	CreditsScroll->DrawTrans( Screen->GetBack() );
	CreditsScroll->ScrollUp(2);

	//Screen->Flip();	 

	if (CreditsScroll->IsTopEnd())
	{
		Screen->FadeToBlack(1000); 
		g_ProgramState = PS_MENU;
    CreditsScroll->ResetPosition();
	}

}


////////////////////////////////////////////////////////////////////////////////////
//Help Menu..............Start
////////////////////////////////////////////////////////////////////////////////////

CDXSprite *sp_Introduction;
CDXSprite *sp_Rules;
CDXSprite *sp_Controls;


void cdx_DoHelpMenu()
{
	
    cdx_HandleHelpMenuKeys();


    Screen->GetBack()->ChangeFont("Verdana", 0, MENU_ITEM_HEIGHT-10, FW_BOLD);
    Screen->GetBack()->SetFont();
    
    switch(HelpMenu->Enter())
    {
        case 0:
            sp_Introduction->Draw(Screen->GetBack( ), 0,	0, CDXBLT_BLK);
            break;
        case 1:
            sp_Rules->Draw(Screen->GetBack( ), 0,	0, CDXBLT_BLK);
            break;
        case 2:
            sp_Controls->Draw(Screen->GetBack( ), 0,	0, CDXBLT_BLK);
            break;
	    
		case 3:
            cdx_DeInitHelpMenu(); 
			return;
            

    }

    

    
    HelpMenu->Draw(50,400);

}




void cdx_InitHelpMenu()
{
    HelpMenu=new CDXMenu();
    HelpMenu->Create(Screen->GetBack(), MENU_ITEM_HEIGHT);


	sp_Introduction=new CDXSprite;
    sp_Introduction->Create(Screen, "./Gfx/introduction.jpg", 1024, 768 , 1 , CDXMEM_SYSTEMONLY);
    sp_Introduction->SetPos(0,0);

	sp_Rules=new CDXSprite;
    sp_Rules->Create(Screen, "./Gfx/rules.jpg", 1024, 768 , 1 , CDXMEM_SYSTEMONLY);
    sp_Rules->SetPos(0,0);

	sp_Controls=new CDXSprite;
    sp_Controls->Create(Screen, "./Gfx/controls.jpg", 1024, 768 , 1 , CDXMEM_SYSTEMONLY);    
    sp_Controls->SetPos(0,0);


    

    Screen->GetBack()->ChangeFont("Verdana", 0, MENU_ITEM_HEIGHT, FW_BOLD);


	  // Add some options to the menu
      HelpMenu->AddItem("Introduction", RGB(255, 0, 0), RGB(255,255, 255));
	  HelpMenu->AddItem("Rules", RGB(255, 0, 0), RGB(255,255, 255));
	  HelpMenu->AddItem("Controls", RGB(255, 0, 0), RGB(255,255, 255));
	  HelpMenu->AddItem("Back", RGB(255, 0, 0), RGB(255,255, 255));

	  

}


void cdx_DeInitHelpMenu()
{

	delete HelpMenu;
	
	delete sp_Introduction;
	delete sp_Rules;
	delete sp_Controls;

    g_ProgramState=PS_MENU;
    Input->Update();
}

void cdx_HandleHelpMenuKeys()
{
    //MainMenu->Home();
    if(Input->GetKeyState(CDXKEY_UPARROW)==CDXKEY_PRESS)
    {
        HelpMenu->Up();
    }
    
    if(Input->GetKeyState(CDXKEY_DOWNARROW)==CDXKEY_PRESS)
    {
        HelpMenu->Down();
    }




}


//////////////////////////////////////////////////////////////////////
//Help Menu..............END
/********************************************************************/
