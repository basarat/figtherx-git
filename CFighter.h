// CFighter.h: interface for the CFighter class.
//
//////////////////////////////////////////////////////////////////////

//#if !defined(AFX_CDXDSPRITE_H__73F8ED27_CA9F_4E37_B240_CC1E4C57391A__INCLUDED_)
//#define AFX_CDXDSPRITE_H__73F8ED27_CA9F_4E37_B240_CC1E4C57391A__INCLUDED_

//#if _MSC_VER > 1000
#pragma once
//#endif // _MSC_VER > 1000

#include <windows.h>
#define CDXINCLUDEALL     // this define includes all headers, otherwise include one by one
#include <cdx.h>
#include "CSprite.h"


typedef int FIGHTERSTATE;
const FIGHTERSTATE FS_STANCE   = 0,
                   FS_WALKF    = 1,
                   FS_WALKB    = 2,
                   FS_JUMPU    = 3,
                   FS_JUMPUF   = 4,
                   FS_JUMPUB   = 5,
                   FS_CROUCH   = 6,
                   FS_FALLING  = 7,
                   FS_LANDING  = 8,
                   FS_PUNCH    = 9,
                   FS_PUNCHU   = 10,
                   FS_PUNCHUF  = 11,
                   FS_PUNCHC   = 12,
                   FS_KICK     = 13,
                   FS_KICKU    = 14,
                   FS_KICKUF   = 15,
                   FS_KICKC    = 16,
                   FS_HADOKEN  = 17,
                   FS_HIT      = 18;   
                    

struct STATEDATA
{
    int iFrames;
    int iStartFrame;   
    int iDelay;
};


class CFighter :public CDXSprite
{
public:
	CFighter();
	
  virtual ~CFighter();
/*
  void SetNumFrames(int num){m_iFrames=num;}

  void SetStartFrame(int num);

  void SetCurFrame(int num);

*/

  BOOL TileHitEx(CDXMap* Map,int Tile);

  
  BOOL SetFighterState(FIGHTERSTATE State);

  void SetFighterStateData(FIGHTERSTATE State,STATEDATA StateData);

  void SetFire(CSprite* ,int Count,int x=0,int y=0 );
  
  void UpdateFire();
  CSprite* GetFire(){return m_pHadokenFire;}



  void SetMap(CDXMap* Map);

  void SetFloorTile(int Tile);

  void SetCollisionRect(RECT);

  void SetWalkVelocity(int velo){m_iWalkVelocity=velo;}
  int GetWalkVelocity(){return m_iWalkVelocity;}

  void SetJumpVelocity(POINT Velo){m_ptJumpVelo=Velo;}
  POINT GetJumpVelocity(){return m_ptJumpVelo;}
  


  void Update();

  void Reverse();
  void Floor();

  void CheckFalling();

  BOOL CheckLeft();
  BOOL CheckRight();
  BOOL CheckUp();
  BOOL CheckDown();

  FIGHTERSTATE GetFighterState();

  void SetPower(int Power){m_iPower=Power;}
  int GetPower(){return m_iPower;}

  RECT GetCollisionRect(){return m_rcCollision;}

  RECT m_rcCollision;

  void ForceState(FIGHTERSTATE State);


  int GetGVelo(){return m_iGVelo;}
  void SetGVelo(int GVelo){m_iGVelo=GVelo;}


  BOOL SetHit(FIGHTERSTATE);
  int m_iImmunity;

  DWORD GetStateTime();
  void SetStateTime(DWORD);

  void SetTotalFrames(int Frames) {      m_iTotalFrames=Frames;  }

  int GetTotalFrames()  {      return m_iTotalFrames;  }

  void SetName(char *name);
  char * GetName(){return m_szName;}


private:
  
    char m_szName[256];


    BOOL StatePossible(FIGHTERSTATE State);
    
    
    FIGHTERSTATE m_fsState;
    
    STATEDATA m_StanceData;
    STATEDATA m_WalkfData;
    STATEDATA m_WalkbData;
    STATEDATA m_JumpuData;
    STATEDATA m_JumpufData;
    STATEDATA m_JumpubData;
    STATEDATA m_CrouchData;
    STATEDATA m_FallingData;
    STATEDATA m_LandingData;
    STATEDATA m_PunchData;
    STATEDATA m_PunchuData;
    STATEDATA m_PunchufData;
    STATEDATA m_PunchcData;
    
    STATEDATA m_KickData;
    STATEDATA m_KickuData;
    STATEDATA m_KickufData;
    STATEDATA m_KickcData;
    
    STATEDATA m_HadokenData;

    CSprite * m_pHadokenFire;
    int m_iHadokenCount;
    int m_iHadokenX;
    int m_iHadokenY;
    



  
    int m_iWalkVelocity;
    POINT m_ptJumpVelo;

    int m_iGravity;
    int m_iGVelo;
    
    //int m_iFrames;
    //int m_iStartFrame;  
    CDXMap* m_Map;
    int m_iFloorTile;

    int m_iCurFrame;
    int m_iFrameTrigger;
   
    int m_iPower;

    DWORD m_StateTimer;

    int m_iTotalFrames;
};

//#endif // !defined(AFX_CDXDSPRITE_H__73F8ED27_CA9F_4E37_B240_CC1E4C57391A__INCLUDED_)
