// CDXDSprite.cpp: implementation of the CDXDSprite class.
//
//////////////////////////////////////////////////////////////////////

#include "CSprite.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSprite::CSprite()
:CDXSprite()
{
    m_bHidden=FALSE;
    m_bOneCycle=FALSE;
    m_iFrames=0;
    m_iCurFrame=0;
    m_iStartFrame=0;

    m_Bounded=FALSE;
    SetRect(&m_BoundRect,0,0,0,0);
    m_iFrameTrigger=0;
    m_iFrameDelay=0;
    
}

CSprite::~CSprite()
{

}



void CSprite::SetStartFrame(int num)
{
    m_iStartFrame=num;
    m_iCurFrame=num;
}


void CSprite::SetCurFrame(int num)
{
    m_iCurFrame=num;
    SetFrame(m_iCurFrame);
}


void CSprite::Update()
{
    if(m_iFrames!=0)
    {
        if(--m_iFrameTrigger<=0)
        {
            m_iFrameTrigger=m_iFrameDelay;


            if((++m_iCurFrame-m_iStartFrame)==m_iFrames)
            {
                m_iCurFrame=m_iStartFrame;
				if(m_bOneCycle)
				{
					SetHidden(TRUE);
				}
            }
            SetFrame(m_iCurFrame);
        }
    }

    int x,y;
    GetVel(x,y);
    if(x!=0)
    {
        SetPosX(GetPosX()+x);
    }
    if(y!=0)
    {
        SetPosY(GetPosY()+y);
    }


    if(m_Bounded)
    {
        if(GetPosX()>m_BoundRect.right)
        {
            if(m_BoundsAction==BA_WRAP)
            {
                SetPosX(m_BoundRect.left);
            }
            else if (m_BoundsAction==BA_HIDE)
            {
                SetHidden(TRUE);
            }
        }

        if(GetPosY()>m_BoundRect.bottom)
        {
            if(m_BoundsAction==BA_WRAP)
            {
                SetPosY(m_BoundRect.top);
            }
            else if (m_BoundsAction==BA_HIDE)
            {
                SetHidden(TRUE);
            }
        }
        if((GetPosX()+GetTile()->GetBlockWidth())<m_BoundRect.left)
        {
            if(m_BoundsAction==BA_WRAP)
            {
                SetPosX(m_BoundRect.right);
            }
            else if (m_BoundsAction==BA_HIDE)
            {
                SetHidden(TRUE);
            }
        }

        if((GetPosY()+GetTile()->GetBlockHeight())<m_BoundRect.top)
        {
            if(m_BoundsAction==BA_WRAP)
            {
                SetPosY(m_BoundRect.bottom);
            }
            else if (m_BoundsAction==BA_HIDE)
            {
                SetHidden(TRUE);
            }
        }
    }

}

void CSprite::Reverse()
{
    int x,y;
    GetVel(x,y);
    if(x!=0)
    {
        SetPosX(GetPosX()-x);
    }
    if(y!=0)
    {
        SetPosY(GetPosY()-y);
    }
}

void CSprite::SetBoundRect(RECT Rc,BOUNDSACTION Action)
{
    CopyRect(&m_BoundRect,&Rc);
    m_Bounded=TRUE;
    m_BoundsAction=Action;
}

void CSprite::SetDelay(int i)
{
    m_iFrameDelay=i;
}