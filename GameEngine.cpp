

//The implementation for the Game Engine

#include "FighterX.h"
#include "CDXBitmapFont.h"
#include "CFighter.h"
#include "StarryBackground.h"
#include "CSprite.h"
#include "TileAnimator.h"



extern CDXHPC GameTime;


extern CDXScreen* Screen;
extern CDXInput*  Input;

extern CDXHPC * timer;

extern CDXBitmapFont* BigFont;

extern StarryBackground* g_Background;
extern CDXMap* Map;

extern CFighter *g_FirstFighter;
extern CFighter *g_FirstFighter1;
extern CFighter *g_FirstFighter2;
extern CDXSprite *g_FirstSplash;



extern CFighter *g_SecondFighter;
extern CFighter *g_SecondFighter1;
extern CFighter *g_SecondFighter2;
extern CDXSprite *g_SecondSplash;



extern CSprite *g_Eye;
extern CSprite *g_Station;
extern CSprite *g_Moon;

extern TileAnimator *g_TileAnimator;
extern TileAnimator *g_TileAnimator2;


extern CSprite *Explosion;


extern int CKEY_RIGHTARROW;
extern int CKEY_LEFTARROW;

//BAS KEYS
extern int CKEY_BASLEFT,
           CKEY_BASRIGHT,
           CKEY_BASUP,
           CKEY_BASDOWN;

//BAS KEYS

extern int g_ProgramState;
extern int g_Winner;



void cdx_AnimateTiles()
{
    g_TileAnimator->Update();
    g_TileAnimator2->Update();
}


void cdx_DrawBackground()
{
  g_Background->Draw();
 

  
  g_Moon->Update();
  g_Moon->Draw(Screen->GetBack(),0,0,CDXBLT_TRANS);

  
  g_Eye->Update();
  g_Eye->Draw(Screen->GetBack(),0,0,CDXBLT_TRANS);

  g_Station->Update();
  g_Station->Draw(Screen->GetBack(),0,0,CDXBLT_TRANS);



  cdx_AnimateTiles();
  
  Map->DrawTrans(Screen->GetBack());
 
}

BOOL cdx_CheckFloorCollision(CFighter*Fighter)
{

    if(Fighter->TileHitEx(Map,52)||Fighter->TileHitEx(Map,117))
    {
       return TRUE;        
    }
    
    return FALSE;
}


void cdx_CheckMapCollision(CFighter*Fighter)
{
    //Hit ground tile will ya!!
    if(cdx_CheckFloorCollision(Fighter))
    {
        Fighter->Floor();        
       // g_FirstFighter->SetFighterState(FS_STANCE);
        //return;
        
    }
    //What you like to hit walls???
    if(Fighter->TileHitEx(Map,168)||Fighter->TileHitEx(Map,158)
        ||Fighter->TileHitEx(Map,148)||Fighter->TileHitEx(Map,48)
        ||Fighter->TileHitEx(Map,179))
    {
        Fighter->Reverse();
        //g_FirstFighter->SetFighterState(FS_STANCE);
        //return;
    }
 
    
}



void cdx_ShowSpritePos()
{
  Screen->GetBack()->GetDC( );

  char Text[256];
	
	sprintf( Text , "Sprite X:%d",g_FirstFighter->GetPosX() );
		
	Screen->GetBack()->TextXY( 10 , 40 , RGB( 255 , 0 , 0 ) , Text );

  sprintf( Text , "Sprite Y: %d",g_FirstFighter->GetPosY() );

  Screen->GetBack()->TextXY( 10 , 60 , RGB( 255 , 0 , 0 ) , Text );

  sprintf(Text ,"Sprite State: %d ",g_FirstFighter->GetFighterState() );

  Screen->GetBack()->TextXY( 10 , 80 , RGB( 255 , 255 , 255 ) , Text );

	Screen->GetBack()->ReleaseDC( );

}


void cdx_DoGame()
{
  cdx_DrawBackground();

  
  
  
  //cdx_ShowSpritePos();

  //cdx_CheckLeftTiles();


  //NOW for the Fighter  
  
  
  g_FirstFighter->Draw( Screen->GetBack( ) , 0 , 0 , CDXBLT_TRANSSHADOWFAST );

  g_SecondFighter->Draw( Screen->GetBack( ) , 0 , 0 , CDXBLT_TRANSSHADOWFAST );


  g_FirstFighter->Draw( Screen->GetBack( ) , 0 , 0 , CDXBLT_TRANS );

  g_SecondFighter->Draw( Screen->GetBack( ) , 0 , 0 , CDXBLT_TRANS );



  //We dont want the player or the Shadow on the left wall now do we....

  RECT ClipRect={0,85,64,768};

  Map->DrawClipped(Screen->GetBack(),&ClipRect);

  if(!g_FirstFighter->GetFire()->IsHidden())
  {
      g_FirstFighter->GetFire()->Draw(Screen->GetBack( ) , 0 , 0 , CDXBLT_TRANSSHADOWFAST );
      g_FirstFighter->GetFire()->Draw(Screen->GetBack(),0,0,CDXBLT_TRANS);
  }
  
  if(!g_SecondFighter->GetFire()->IsHidden())
  {
      g_SecondFighter->GetFire()->Draw(Screen->GetBack( ) , 0 , 0 , CDXBLT_TRANSSHADOWFAST );
      g_SecondFighter->GetFire()->Draw(Screen->GetBack(),0,0,CDXBLT_TRANS);
  }

  //Explosion->SetHidden(FALSE);
  Explosion->Update();

  if(!Explosion->IsHidden())
  {
	  
	  Explosion->Draw(Screen->GetBack( ) , 0 , 0 , CDXBLT_TRANS );
      //g_SecondFighter->GetFire()->Draw(Screen->GetBack(),0,0,CDXBLT_TRANS);

  }


  

  


  cdx_DrawStats();



  

  if(g_ProgramState==PS_GAME)
  {
      g_FirstFighter->Update();
      g_SecondFighter->Update();
  }

  cdx_CheckSpriteCollisions();



  //cdx_CheckMapCollision();
}





// ------------------------------------------------------------------
// cdx_HandleKeys - Handels user input
// ------------------------------------------------------------------
void cdx_HandleKeys()
{
    //cdx_CheckMapCollision();



    if(Input->GetKeyState(CDXKEY_SPACE))
    {
        SetFrameRate(BULLET_TIME);
        
    }
    else 
        SetFrameRate(NORMAL_TIME);


 
    if(Input->GetKeyState(CDXKEY_RIGHTCTRL)==CDXKEY_PRESS)
    {
        cdx_BlChangeFacing();
    }

    
    //Input->Update();


    
    if(Input->GetKeyState(CDXKEY_J))
    {
 
        g_FirstFighter->SetFighterState(FS_HADOKEN);
            return;

    }

    if((Input->GetKeyState(CDXKEY_UPARROW))
        &&(Input->GetKeyState(CDXKEY_K))
        &&(Input->GetKeyState(CKEY_RIGHTARROW))   )
    {
        if(g_FirstFighter->SetFighterState(FS_KICKUF))
            g_FirstFighter->SetStateTime(timer->GetValue());
        return;
    }

    if((Input->GetKeyState(CDXKEY_UPARROW))
        &&(Input->GetKeyState(CDXKEY_L))
        &&(Input->GetKeyState(CKEY_RIGHTARROW))   )
    {
        if(g_FirstFighter->SetFighterState(FS_PUNCHUF))
        {
            g_FirstFighter->SetStateTime(timer->GetValue());
        }
        return;
    }
    

    if((Input->GetKeyState(CDXKEY_UPARROW))
        &&(Input->GetKeyState(CDXKEY_L)))
    {
        if(g_FirstFighter->SetFighterState(FS_PUNCHU))
        {
            g_FirstFighter->SetStateTime(timer->GetValue());
        }
        return;
    }
    
    if((Input->GetKeyState(CDXKEY_UPARROW))
        &&(Input->GetKeyState(CDXKEY_K)))
    {
        if(g_FirstFighter->SetFighterState(FS_KICKU))
        {
            g_FirstFighter->SetStateTime(timer->GetValue());
        }
        return;
    }


    if((Input->GetKeyState(CDXKEY_DOWNARROW))
        &&(Input->GetKeyState(CDXKEY_K)))
    {
        if(g_FirstFighter->SetFighterState(FS_KICKC))
        {
            g_FirstFighter->SetStateTime(timer->GetValue());
        }
        return;
    }


    if((Input->GetKeyState(CDXKEY_DOWNARROW))
        &&(Input->GetKeyState(CDXKEY_L)))
    {
        if(g_FirstFighter->SetFighterState(FS_PUNCHC))
        {
            g_FirstFighter->SetStateTime(timer->GetValue());
        }
        return;
    }


    

    if(Input->GetKeyState(CDXKEY_L))
    {
        if(g_FirstFighter->SetFighterState(FS_PUNCH))
        {
            g_FirstFighter->SetStateTime(timer->GetValue());
        }
        return;
    }
    

    if(Input->GetKeyState(CDXKEY_K))
    {
        if(g_FirstFighter->SetFighterState(FS_KICK))
        {
            g_FirstFighter->SetStateTime(timer->GetValue());
        }
        return;
    }
    

    if((Input->GetKeyState(CDXKEY_UPARROW))
        &&(Input->GetKeyState(CKEY_RIGHTARROW)))
    {
        g_FirstFighter->SetFighterState(FS_JUMPUF);
        return;
    }
    
    if((Input->GetKeyState(CDXKEY_UPARROW))
        &&(Input->GetKeyState(CKEY_LEFTARROW)))
    {
        g_FirstFighter->SetFighterState(FS_JUMPUB);
        return;
    }
    

    if(Input->GetKeyState(CDXKEY_UPARROW))
    {
        g_FirstFighter->SetFighterState(FS_JUMPU);
        return;
    }
    
    if(Input->GetKeyState(CKEY_RIGHTARROW))
    {
        g_FirstFighter->SetFighterState(FS_WALKF);
        return;
    }

    if(Input->GetKeyState(CKEY_LEFTARROW))
    {
        g_FirstFighter->SetFighterState(FS_WALKB);
        return;
    }

    if(Input->GetKeyState(CDXKEY_DOWNARROW))
    {
        g_FirstFighter->SetFighterState(FS_CROUCH);
        return;
    }


    g_FirstFighter->SetFighterState(FS_STANCE);



}


void cdx_HandleP2Keys()
{
    //////////////////////////////////////////////////////////////////////
   
    if(Input->GetKeyState(CDXKEY_LEFTCTRL)==CDXKEY_PRESS)
    {
        cdx_BasChangeFacing();
    }


    if(Input->GetKeyState(CDXKEY_1))
    {
 
        g_SecondFighter->SetFighterState(FS_HADOKEN);
            return;

    }

    if((Input->GetKeyState(CKEY_BASUP))
        &&(Input->GetKeyState(CDXKEY_2))
        &&(Input->GetKeyState(CKEY_BASRIGHT))   )
    {
        if(g_SecondFighter->SetFighterState(FS_KICKUF))
        {
            g_SecondFighter->SetStateTime(timer->GetValue());
        }
        return;
    }

    if((Input->GetKeyState(CKEY_BASUP))
        &&(Input->GetKeyState(CDXKEY_3))
        &&(Input->GetKeyState(CKEY_BASRIGHT))   )
    {
        if(g_SecondFighter->SetFighterState(FS_PUNCHUF))
        {
            g_SecondFighter->SetStateTime(timer->GetValue());
        }
        return;
    }
    

    if((Input->GetKeyState(CKEY_BASUP))
        &&(Input->GetKeyState(CDXKEY_3)))
    {
        if(g_SecondFighter->SetFighterState(FS_PUNCHU))
        {
            g_SecondFighter->SetStateTime(timer->GetValue());
        }
        return;
    }
    
    if((Input->GetKeyState(CKEY_BASUP))
        &&(Input->GetKeyState(CDXKEY_2)))
    {
        if(g_SecondFighter->SetFighterState(FS_KICKU))
        {
            g_SecondFighter->SetStateTime(timer->GetValue());
        }
        return;
    }


    if((Input->GetKeyState(CKEY_BASDOWN))
        &&(Input->GetKeyState(CDXKEY_2)))
    {
        if(g_SecondFighter->SetFighterState(FS_KICKC))
        {        
            g_SecondFighter->SetStateTime(timer->GetValue());
        }
        return;
    }


    if((Input->GetKeyState(CKEY_BASDOWN))
        &&(Input->GetKeyState(CDXKEY_3)))
    {
        if(g_SecondFighter->SetFighterState(FS_PUNCHC))
        {
            g_SecondFighter->SetStateTime(timer->GetValue());
        }
        return;
    }


    

    if(Input->GetKeyState(CDXKEY_3))
    {
        if(g_SecondFighter->SetFighterState(FS_PUNCH))
        {
            g_SecondFighter->SetStateTime(timer->GetValue());
        }
        return;
    }
    

    if(Input->GetKeyState(CDXKEY_2))
    {
        if(g_SecondFighter->SetFighterState(FS_KICK))
        {
            g_SecondFighter->SetStateTime(timer->GetValue());
        }
        return;
    }
    

    if((Input->GetKeyState(CKEY_BASUP))
        &&(Input->GetKeyState(CKEY_BASRIGHT)))
    {
        g_SecondFighter->SetFighterState(FS_JUMPUF);
        return;
    }
    
    if((Input->GetKeyState(CKEY_BASUP))
        &&(Input->GetKeyState(CKEY_BASLEFT)))
    {
        g_SecondFighter->SetFighterState(FS_JUMPUB);
        return;
    }
    

    if(Input->GetKeyState(CKEY_BASUP))
    {
        g_SecondFighter->SetFighterState(FS_JUMPU);
        return;
    }
    
    if(Input->GetKeyState(CKEY_BASRIGHT))
    {
        g_SecondFighter->SetFighterState(FS_WALKF);
        return;
    }

    if(Input->GetKeyState(CKEY_BASLEFT))
    {
        g_SecondFighter->SetFighterState(FS_WALKB);
        return;
    }

    if(Input->GetKeyState(CKEY_BASDOWN))
    {
        g_SecondFighter->SetFighterState(FS_CROUCH);
        return;
    }

    g_SecondFighter->SetFighterState(FS_STANCE);
}


void cdx_DrawStats()
{


    int x=30,y=110;

    int x2=815;

    int x3=432;

    Screen->GetBack()->ChangeFont( "Verdana" , 0 , 12 , FW_BOLD);
    Screen->GetBack()->GetDC();
    Screen->GetBack()->SetFont();
    //Screen->GetBack()->TextXY(x-57,y,0x0000FF,"Player One");
    Screen->GetBack()->TextXY(x+2*(100)+1,y,0x0000FF,g_FirstFighter->GetName());
    Screen->GetBack()->TextXY((x2-5*strlen(g_SecondFighter->GetName())-2),y,0x0000FF,g_SecondFighter->GetName());
    Screen->GetBack()->TextXY(x3-57,y,RGB(255,0,0),"Game Time");
    Screen->GetBack()->ReleaseDC();

    if(g_FirstFighter->GetPower()<=0)
    {
        g_Winner=2;
        g_ProgramState=PS_END;
    }
    else if(g_SecondFighter->GetPower()<=0)
    {
        g_Winner=1;
        g_ProgramState=PS_END;
    }

    Screen->GetBack()->FillRect(x,y,x+(100)*2,y+10,0xFFFFFF);
    Screen->GetBack()->FillRect(x,y,x+(g_FirstFighter->GetPower())*2,y+10,0xFF0000);

    Screen->GetBack()->FillRect(x2,y,x2+(100)*2,y+10,0xFFFFFF);
    Screen->GetBack()->FillRect(x2,y,x2+(g_SecondFighter->GetPower())*2,y+10,0xFF0000);

    Screen->GetBack()->FillRect(x3-1,y-1,x3+(100)*2+1,y+10+1,0xFF0000);
    

    if((GameTime.GetValue()/1000)>200)
    {
        
        Screen->GetBack()->FillRect(x3,y,x3+(100)*2,y+10,RGB(255,255,255));
        //GameTime.Wait(10000);
      if(g_ProgramState!=PS_END)
      {
        if(g_FirstFighter->GetPower()>=g_SecondFighter->GetPower())
        {
            g_Winner=2;
            g_ProgramState=PS_END;
        }
        else if(g_FirstFighter->GetPower()<g_SecondFighter->GetPower())
        {
            g_Winner=1;
            g_ProgramState=PS_END;
        }
      }

    }
    else
    {
        Screen->GetBack()->FillRect(x3,y,x3+(GameTime.GetValue())/1000,y+10,RGB(255,255,255));
    }

    
}

/*
FS_STANCE   = 0,
                   FS_WALKF    = 1,
                   FS_WALKB    = 2,
                   FS_JUMPU    = 3,
                   FS_JUMPUF   = 4,
                   FS_JUMPUB   = 5,
                   FS_CROUCH   = 6,
                   FS_FALLING  = 7,
                   FS_LANDING  = 8,
                   FS_PUNCH    = 9,
                   FS_PUNCHU   = 10,
                   FS_PUNCHUF  = 11,
                   FS_PUNCHC   = 12,
                   FS_KICK     = 13,
                   FS_KICKU    = 14,
                   FS_KICKUF   = 15,
                   FS_KICKC    = 16,
                   FS_HADOKEN  = 17,
                   FS_HIT      = 18;   
*/                   

void cdx_FaceEachother()
{

    int InputSwapper;
    int State;

    if(  ((g_FirstFighter->GetPosX()+g_FirstFighter->GetCollisionRect().left)>
        (g_SecondFighter->GetPosX()+g_SecondFighter->GetCollisionRect().left )  ))  //&&
                 //((g_FirstFighter->GetPosX()/*+g_FirstFighter->GetCollisionRect().left*/)<524))
    {

        if((g_FirstFighter==g_FirstFighter2)&&(g_SecondFighter==g_SecondFighter1))
            return;
      
        State=g_FirstFighter->GetFighterState();
        
        if(State==FS_STANCE||State==FS_CROUCH||State==FS_FALLING
            ||State==FS_LANDING||State==FS_PUNCH||State==FS_KICK
            ||State==FS_PUNCHC||State==FS_KICKC||State==FS_HADOKEN
            ||State==FS_JUMPU||State==FS_PUNCHU||State==FS_KICKU)
        {

            g_FirstFighter2->SetPosX(g_FirstFighter->GetPosX()-g_FirstFighter2->GetCollisionRect().left
            +g_FirstFighter->GetCollisionRect().left);
        
        }
        if(State==FS_WALKF)
        {
            g_FirstFighter2->SetPosX(g_FirstFighter->GetPosX()-g_FirstFighter2->GetCollisionRect().left
            +g_FirstFighter->GetCollisionRect().left-g_FirstFighter2->GetWalkVelocity());
        }
        if(State==FS_WALKB)
        {
           g_FirstFighter2->SetPosX(g_FirstFighter->GetPosX()-g_FirstFighter2->GetCollisionRect().left
           +g_FirstFighter->GetCollisionRect().left+g_FirstFighter2->GetWalkVelocity());
   
        }
        if(State==FS_JUMPUB)
        {
            g_FirstFighter2->SetPosX(g_FirstFighter->GetPosX()-g_FirstFighter2->GetCollisionRect().left
            +g_FirstFighter->GetCollisionRect().left+g_FirstFighter2->GetJumpVelocity().x);
        
        }
        if(State==FS_JUMPUF||State==FS_KICKUF||State==FS_PUNCHUF)
        {
             g_FirstFighter2->SetPosX(g_FirstFighter->GetPosX()-g_FirstFighter2->GetCollisionRect().left
            +g_FirstFighter->GetCollisionRect().left-g_FirstFighter2->GetJumpVelocity().x);
        }



        
        g_FirstFighter2->SetPosY(g_FirstFighter->GetPosY());

        g_FirstFighter2->SetGVelo(g_FirstFighter->GetGVelo());
        
        g_FirstFighter2->ForceState(g_FirstFighter->GetFighterState());

        g_FirstFighter2->SetPower(g_FirstFighter->GetPower());

        g_FirstFighter2->GetFire()->SetHidden(TRUE);//g_FirstFighter->GetFire()->IsHidden());

        //g_FirstFighter2->GetFire()->SetPos(g_FirstFighter->GetFire()->GetPosX(),g_FirstFighter->GetFire()->GetPosY());

        g_FirstFighter2->SetStateTime(g_FirstFighter->GetStateTime());

        g_FirstFighter=g_FirstFighter2;



        State=g_SecondFighter->GetFighterState();
        
        if(State==FS_STANCE||State==FS_CROUCH||State==FS_FALLING
            ||State==FS_LANDING||State==FS_PUNCH||State==FS_KICK
            ||State==FS_PUNCHC||State==FS_KICKC||State==FS_HADOKEN
            ||State==FS_JUMPU||State==FS_PUNCHU||State==FS_KICKU)
        {

            g_SecondFighter1->SetPosX(g_SecondFighter->GetPosX()-g_SecondFighter1->GetCollisionRect().left
            +g_SecondFighter->GetCollisionRect().left);
        
        }
        if(State==FS_WALKF)
        {
            g_SecondFighter1->SetPosX(g_SecondFighter->GetPosX()-g_SecondFighter1->GetCollisionRect().left
            +g_SecondFighter->GetCollisionRect().left-g_SecondFighter1->GetWalkVelocity());
        }
        if(State==FS_WALKB)
        {
           g_SecondFighter1->SetPosX(g_SecondFighter->GetPosX()-g_SecondFighter1->GetCollisionRect().left
           +g_SecondFighter->GetCollisionRect().left+g_SecondFighter1->GetWalkVelocity());
   
        }
        if(State==FS_JUMPUB)
        {
            g_SecondFighter1->SetPosX(g_SecondFighter->GetPosX()-g_SecondFighter1->GetCollisionRect().left
            +g_SecondFighter->GetCollisionRect().left+g_SecondFighter1->GetJumpVelocity().x);
        
        }
        if(State==FS_JUMPUF||State==FS_KICKUF||State==FS_PUNCHUF)
        {
             g_SecondFighter1->SetPosX(g_SecondFighter->GetPosX()-g_SecondFighter1->GetCollisionRect().left
            +g_SecondFighter->GetCollisionRect().left-g_SecondFighter1->GetJumpVelocity().x);
        }




        g_SecondFighter1->SetPosY(g_SecondFighter->GetPosY());

        g_SecondFighter1->SetGVelo(g_SecondFighter->GetGVelo());

        g_SecondFighter1->ForceState(g_SecondFighter->GetFighterState());


        g_SecondFighter1->SetPower(g_SecondFighter->GetPower());

        g_SecondFighter1->SetStateTime(g_SecondFighter->GetStateTime());


        g_SecondFighter1->GetFire()->SetHidden(TRUE);//g_FirstFighter->GetFire()->IsHidden());

        g_SecondFighter=g_SecondFighter1;

        InputSwapper=CKEY_BASLEFT;
        CKEY_BASLEFT=CKEY_BASRIGHT;
        CKEY_BASRIGHT=InputSwapper;





        CKEY_RIGHTARROW=CDXKEY_LEFTARROW;
        CKEY_LEFTARROW=CDXKEY_RIGHTARROW;


    }
    else
    {
        if((g_FirstFighter==g_FirstFighter1)&&(g_SecondFighter==g_SecondFighter2))
            return;


        State=g_FirstFighter->GetFighterState();
        
        if(State==FS_STANCE||State==FS_CROUCH||State==FS_FALLING
            ||State==FS_LANDING||State==FS_PUNCH||State==FS_KICK
            ||State==FS_PUNCHC||State==FS_KICKC||State==FS_HADOKEN
            ||State==FS_JUMPU||State==FS_PUNCHU||State==FS_KICKU)
        {

            g_FirstFighter1->SetPosX(g_FirstFighter->GetPosX()-g_FirstFighter1->GetCollisionRect().left
            +g_FirstFighter->GetCollisionRect().left);
        
        }
        if(State==FS_WALKF)
        {
            g_FirstFighter1->SetPosX(g_FirstFighter->GetPosX()-g_FirstFighter1->GetCollisionRect().left
            +g_FirstFighter->GetCollisionRect().left-g_FirstFighter1->GetWalkVelocity());
        }
        if(State==FS_WALKB)
        {
           g_FirstFighter1->SetPosX(g_FirstFighter->GetPosX()-g_FirstFighter1->GetCollisionRect().left
           +g_FirstFighter->GetCollisionRect().left+g_FirstFighter1->GetWalkVelocity());
   
        }
        if(State==FS_JUMPUB)
        {
            g_FirstFighter1->SetPosX(g_FirstFighter->GetPosX()-g_FirstFighter1->GetCollisionRect().left
            +g_FirstFighter->GetCollisionRect().left+g_FirstFighter1->GetJumpVelocity().x);
        
        }
        if(State==FS_JUMPUF||State==FS_KICKUF||State==FS_PUNCHUF)
        {
             g_FirstFighter1->SetPosX(g_FirstFighter->GetPosX()-g_FirstFighter1->GetCollisionRect().left
            +g_FirstFighter->GetCollisionRect().left-g_FirstFighter1->GetJumpVelocity().x);
        }


        
        
        
        g_FirstFighter1->SetPosY(g_FirstFighter->GetPosY());

        g_FirstFighter1->SetGVelo(g_FirstFighter->GetGVelo());

        g_FirstFighter1->ForceState(g_FirstFighter->GetFighterState());


        g_FirstFighter1->SetPower(g_FirstFighter->GetPower());

        g_FirstFighter1->GetFire()->SetHidden(TRUE);//g_FirstFighter->GetFire()->IsHidden());

        //g_FirstFighter1->GetFire()->SetPos(g_FirstFighter->GetFire()->GetPosX(),g_FirstFighter->GetFire()->GetPosY());

        g_FirstFighter1->SetStateTime(g_FirstFighter->GetStateTime());
        
        g_FirstFighter=g_FirstFighter1;



        State=g_SecondFighter->GetFighterState();
        
        if(State==FS_STANCE||State==FS_CROUCH||State==FS_FALLING
            ||State==FS_LANDING||State==FS_PUNCH||State==FS_KICK
            ||State==FS_PUNCHC||State==FS_KICKC||State==FS_HADOKEN
            ||State==FS_JUMPU||State==FS_PUNCHU||State==FS_KICKU)
        {

            g_SecondFighter2->SetPosX(g_SecondFighter->GetPosX()-g_SecondFighter2->GetCollisionRect().left
            +g_SecondFighter->GetCollisionRect().left);
        
        }
        if(State==FS_WALKF)
        {
            g_SecondFighter2->SetPosX(g_SecondFighter->GetPosX()-g_SecondFighter2->GetCollisionRect().left
            +g_SecondFighter->GetCollisionRect().left-g_SecondFighter2->GetWalkVelocity());
        }
        if(State==FS_WALKB)
        {
           g_SecondFighter2->SetPosX(g_SecondFighter->GetPosX()-g_SecondFighter2->GetCollisionRect().left
           +g_SecondFighter->GetCollisionRect().left+g_SecondFighter2->GetWalkVelocity());
   
        }
        if(State==FS_JUMPUB)
        {
            g_SecondFighter2->SetPosX(g_SecondFighter->GetPosX()-g_SecondFighter2->GetCollisionRect().left
            +g_SecondFighter->GetCollisionRect().left+g_SecondFighter2->GetJumpVelocity().x);
        
        }
        if(State==FS_JUMPUF||State==FS_KICKUF||State==FS_PUNCHUF)
        {
             g_SecondFighter2->SetPosX(g_SecondFighter->GetPosX()-g_SecondFighter2->GetCollisionRect().left
            +g_SecondFighter->GetCollisionRect().left-g_SecondFighter2->GetJumpVelocity().x);
        }


        
        g_SecondFighter2->SetPosY(g_SecondFighter->GetPosY());

        g_SecondFighter2->SetGVelo(g_SecondFighter->GetGVelo());
        
        g_SecondFighter2->ForceState(g_SecondFighter->GetFighterState());

        g_SecondFighter2->SetPower(g_SecondFighter->GetPower());

        g_SecondFighter2->GetFire()->SetHidden(TRUE);//g_FirstFighter->GetFire()->IsHidden());

        //g_FirstFighter2->GetFire()->SetPos(g_FirstFighter->GetFire()->GetPosX(),g_FirstFighter->GetFire()->GetPosY());

        g_SecondFighter2->SetStateTime(g_SecondFighter->GetStateTime());

        g_SecondFighter=g_SecondFighter2;


        
        InputSwapper=CKEY_BASLEFT;
        CKEY_BASLEFT=CKEY_BASRIGHT;
        CKEY_BASRIGHT=InputSwapper;



        CKEY_RIGHTARROW=CDXKEY_RIGHTARROW;
        CKEY_LEFTARROW=CDXKEY_LEFTARROW;
    }
     
    
}


void cdx_TestRun()
{

    g_FirstFighter1->SetPos(0,0);
    g_FirstFighter1->GetFire()->SetPos(150,0);
    g_FirstFighter2->SetPos(892-g_FirstFighter2->GetCollisionRect().left,0);
    g_FirstFighter2->GetFire()->SetPos(772,0);

    g_FirstSplash->SetPos(270,69);

    g_SecondFighter1->SetPos(0,453);
    g_SecondFighter1->GetFire()->SetPos(150,453);
    g_SecondFighter2->SetPos(892-g_SecondFighter2->GetCollisionRect().left,453);
    g_SecondFighter2->GetFire()->SetPos(772,453);

    g_SecondSplash->SetPos(270,453);


    
    for(int i=0;i<max(g_FirstFighter->GetTotalFrames(),g_SecondFighter->GetTotalFrames());i++)
    {
        //::Sleep(100);
        
        Screen->GetBack()->Fill(0);

        g_FirstSplash->Draw(Screen->GetBack() , 0 , 0 , CDXBLT_TRANS);

        g_SecondSplash->Draw(Screen->GetBack() , 0 , 0 , CDXBLT_TRANS);

        BigFont->DrawTrans(500,370,"Vs.",Screen->GetBack());


        if(i<g_FirstFighter->GetTotalFrames())
        {
            g_FirstFighter2->SetFrame(i);
        
            g_FirstFighter2->Draw( Screen->GetBack( ) , 0 , 0 , CDXBLT_TRANS );

            g_FirstFighter1->SetFrame(i);
        
            g_FirstFighter1->Draw( Screen->GetBack( ) , 0 , 0 , CDXBLT_TRANS );

        
        }

        if(i<g_SecondFighter->GetTotalFrames())

        {
            g_SecondFighter2->SetFrame(i);
        
            g_SecondFighter2->Draw( Screen->GetBack( ) , 0 , 0 , CDXBLT_TRANS );

            g_SecondFighter1->SetFrame(i);
        
            g_SecondFighter1->Draw( Screen->GetBack( ) , 0 , 0 , CDXBLT_TRANS );
        }

        if(i<g_FirstFighter->GetFire()->GetNumFrames())
        {
            g_FirstFighter1->GetFire()->SetFrame(i);

            g_FirstFighter1->GetFire()->Draw( Screen->GetBack( ) , 0 , 0 , CDXBLT_TRANS );
            
            g_FirstFighter2->GetFire()->SetFrame(i);

            g_FirstFighter2->GetFire()->Draw( Screen->GetBack( ) , 0 , 0 , CDXBLT_TRANS );
        }
        
        if(i<g_SecondFighter->GetFire()->GetNumFrames())
        {
            g_SecondFighter1->GetFire()->SetFrame(i);

            g_SecondFighter1->GetFire()->Draw( Screen->GetBack( ) , 0 , 0 , CDXBLT_TRANS );
            
            g_SecondFighter2->GetFire()->SetFrame(i);

            g_SecondFighter2->GetFire()->Draw( Screen->GetBack( ) , 0 , 0 , CDXBLT_TRANS );
        }



        Screen->Flip();
    }

    g_FirstFighter->SetPos(300,503);
    g_SecondFighter->SetPos(500,503);

    g_FirstFighter->ForceState(FS_STANCE);
    g_SecondFighter->ForceState(FS_STANCE);

}








void cdx_RestartGame()
{
    g_FirstFighter=g_FirstFighter1;
    CKEY_LEFTARROW=CDXKEY_LEFTARROW;
    CKEY_RIGHTARROW=CDXKEY_RIGHTARROW;

    g_SecondFighter=g_SecondFighter2;
    CKEY_BASLEFT=CDXKEY_D;
    CKEY_BASRIGHT=CDXKEY_A;


    g_FirstFighter->SetPos(128,450);
    g_SecondFighter->SetPos(700,450);

    g_FirstFighter->GetFire()->SetHidden(TRUE);
    g_SecondFighter->GetFire()->SetHidden(TRUE);

    g_FirstFighter->ForceState(FS_STANCE);
    g_SecondFighter->ForceState(FS_STANCE);

    g_FirstFighter->SetPower(100);
    g_SecondFighter->SetPower(100);

    g_ProgramState=PS_GAME;

    cdx_TestRun();

    GameTime.Reset();

}

void cdx_BlChangeFacing()
{
//    int InputSwapper;
    int State;

    if(g_FirstFighter!=g_FirstFighter2)
    {


        
/*    
      
        State=g_FirstFighter->GetFighterState();
        
        if(State==FS_STANCE||State==FS_CROUCH||State==FS_FALLING
            ||State==FS_LANDING||State==FS_PUNCH||State==FS_KICK
            ||State==FS_PUNCHC||State==FS_KICKC||State==FS_HADOKEN
            ||State==FS_JUMPU||State==FS_PUNCHU||State==FS_KICKU)
        {

            g_FirstFighter2->SetPosX(g_FirstFighter->GetPosX()-g_FirstFighter2->GetCollisionRect().left
            +g_FirstFighter->GetCollisionRect().left);
        
        }
        if(State==FS_WALKF)
        {
            g_FirstFighter2->SetPosX(g_FirstFighter->GetPosX()-g_FirstFighter2->GetCollisionRect().left
            +g_FirstFighter->GetCollisionRect().left-g_FirstFighter2->GetWalkVelocity());
        }
        if(State==FS_WALKB)
        {
           g_FirstFighter2->SetPosX(g_FirstFighter->GetPosX()-g_FirstFighter2->GetCollisionRect().left
           +g_FirstFighter->GetCollisionRect().left+g_FirstFighter2->GetWalkVelocity());
   
        }
        if(State==FS_JUMPUB)
        {
            g_FirstFighter2->SetPosX(g_FirstFighter->GetPosX()-g_FirstFighter2->GetCollisionRect().left
            +g_FirstFighter->GetCollisionRect().left+g_FirstFighter2->GetJumpVelocity().x);
        
        }
        if(State==FS_JUMPUF||State==FS_KICKUF||State==FS_PUNCHUF)
        {
             g_FirstFighter2->SetPosX(g_FirstFighter->GetPosX()-g_FirstFighter2->GetCollisionRect().left
            +g_FirstFighter->GetCollisionRect().left-g_FirstFighter2->GetJumpVelocity().x);
        }

*/

        
       g_FirstFighter2->SetPosX(g_FirstFighter->GetPosX()-g_FirstFighter2->GetCollisionRect().left
            +g_FirstFighter->GetCollisionRect().left);
  

        
        g_FirstFighter2->SetPosY(g_FirstFighter->GetPosY());

        g_FirstFighter2->SetGVelo(g_FirstFighter->GetGVelo());
        
        g_FirstFighter2->ForceState(g_FirstFighter->GetFighterState());

        g_FirstFighter2->SetPower(g_FirstFighter->GetPower());

        g_FirstFighter2->GetFire()->SetHidden(TRUE);//g_FirstFighter->GetFire()->IsHidden());

        //g_FirstFighter2->GetFire()->SetPos(g_FirstFighter->GetFire()->GetPosX(),g_FirstFighter->GetFire()->GetPosY());

        g_FirstFighter2->SetStateTime(g_FirstFighter->GetStateTime());

        g_FirstFighter=g_FirstFighter2;


        CKEY_RIGHTARROW=CDXKEY_LEFTARROW;
        CKEY_LEFTARROW=CDXKEY_RIGHTARROW;


    }
    else
    {
        State=g_FirstFighter->GetFighterState();
  
/*        
        if(State==FS_STANCE||State==FS_CROUCH||State==FS_FALLING
            ||State==FS_LANDING||State==FS_PUNCH||State==FS_KICK
            ||State==FS_PUNCHC||State==FS_KICKC||State==FS_HADOKEN
            ||State==FS_JUMPU||State==FS_PUNCHU||State==FS_KICKU)
        {

            g_FirstFighter1->SetPosX(g_FirstFighter->GetPosX()-g_FirstFighter1->GetCollisionRect().left
            +g_FirstFighter->GetCollisionRect().left);
        
        }
        if(State==FS_WALKF)
        {
            g_FirstFighter1->SetPosX(g_FirstFighter->GetPosX()-g_FirstFighter1->GetCollisionRect().left
            +g_FirstFighter->GetCollisionRect().left-g_FirstFighter1->GetWalkVelocity());
        }
        if(State==FS_WALKB)
        {
           g_FirstFighter1->SetPosX(g_FirstFighter->GetPosX()-g_FirstFighter1->GetCollisionRect().left
           +g_FirstFighter->GetCollisionRect().left+g_FirstFighter1->GetWalkVelocity());
   
        }
        if(State==FS_JUMPUB)
        {
            g_FirstFighter1->SetPosX(g_FirstFighter->GetPosX()-g_FirstFighter1->GetCollisionRect().left
            +g_FirstFighter->GetCollisionRect().left+g_FirstFighter1->GetJumpVelocity().x);
        
        }
        if(State==FS_JUMPUF||State==FS_KICKUF||State==FS_PUNCHUF)
        {
             g_FirstFighter1->SetPosX(g_FirstFighter->GetPosX()-g_FirstFighter1->GetCollisionRect().left
            +g_FirstFighter->GetCollisionRect().left-g_FirstFighter1->GetJumpVelocity().x);
        }


        
*/
        
        g_FirstFighter1->SetPosX(g_FirstFighter->GetPosX()-g_FirstFighter1->GetCollisionRect().left
            +g_FirstFighter->GetCollisionRect().left);
  
        
        g_FirstFighter1->SetPosY(g_FirstFighter->GetPosY());

        g_FirstFighter1->SetGVelo(g_FirstFighter->GetGVelo());

        g_FirstFighter1->ForceState(g_FirstFighter->GetFighterState());


        g_FirstFighter1->SetPower(g_FirstFighter->GetPower());

        g_FirstFighter1->GetFire()->SetHidden(TRUE);//g_FirstFighter->GetFire()->IsHidden());

        //g_FirstFighter1->GetFire()->SetPos(g_FirstFighter->GetFire()->GetPosX(),g_FirstFighter->GetFire()->GetPosY());

        g_FirstFighter1->SetStateTime(g_FirstFighter->GetStateTime());
        
        g_FirstFighter=g_FirstFighter1;


        CKEY_RIGHTARROW=CDXKEY_RIGHTARROW;
        CKEY_LEFTARROW=CDXKEY_LEFTARROW;
    }
   
}

void cdx_BasChangeFacing()
{
    
    int InputSwapper;
    int State;

    if(g_SecondFighter!=g_SecondFighter1)
    {
  
        State=g_SecondFighter->GetFighterState();
    /*    
        if(State==FS_STANCE||State==FS_CROUCH||State==FS_FALLING
            ||State==FS_LANDING||State==FS_PUNCH||State==FS_KICK
            ||State==FS_PUNCHC||State==FS_KICKC||State==FS_HADOKEN
            ||State==FS_JUMPU||State==FS_PUNCHU||State==FS_KICKU)
        {

            g_SecondFighter1->SetPosX(g_SecondFighter->GetPosX()-g_SecondFighter1->GetCollisionRect().left
            +g_SecondFighter->GetCollisionRect().left);
        
        }
        if(State==FS_WALKF)
        {
            g_SecondFighter1->SetPosX(g_SecondFighter->GetPosX()-g_SecondFighter1->GetCollisionRect().left
            +g_SecondFighter->GetCollisionRect().left-g_SecondFighter1->GetWalkVelocity());
        }
        if(State==FS_WALKB)
        {
           g_SecondFighter1->SetPosX(g_SecondFighter->GetPosX()-g_SecondFighter1->GetCollisionRect().left
           +g_SecondFighter->GetCollisionRect().left+g_SecondFighter1->GetWalkVelocity());
   
        }
        if(State==FS_JUMPUB)
        {
            g_SecondFighter1->SetPosX(g_SecondFighter->GetPosX()-g_SecondFighter1->GetCollisionRect().left
            +g_SecondFighter->GetCollisionRect().left+g_SecondFighter1->GetJumpVelocity().x);
        
        }
        if(State==FS_JUMPUF||State==FS_KICKUF||State==FS_PUNCHUF)
        {
             g_SecondFighter1->SetPosX(g_SecondFighter->GetPosX()-g_SecondFighter1->GetCollisionRect().left
            +g_SecondFighter->GetCollisionRect().left-g_SecondFighter1->GetJumpVelocity().x);
        }

  */

        g_SecondFighter1->SetPosX(g_SecondFighter->GetPosX()-g_SecondFighter1->GetCollisionRect().left
            +g_SecondFighter->GetCollisionRect().left);
    


        g_SecondFighter1->SetPosY(g_SecondFighter->GetPosY());

        g_SecondFighter1->SetGVelo(g_SecondFighter->GetGVelo());

        g_SecondFighter1->ForceState(g_SecondFighter->GetFighterState());


        g_SecondFighter1->SetPower(g_SecondFighter->GetPower());

        g_SecondFighter1->SetStateTime(g_SecondFighter->GetStateTime());


        g_SecondFighter1->GetFire()->SetHidden(TRUE);//g_FirstFighter->GetFire()->IsHidden());

        g_SecondFighter=g_SecondFighter1;

        InputSwapper=CKEY_BASLEFT;
        CKEY_BASLEFT=CKEY_BASRIGHT;
        CKEY_BASRIGHT=InputSwapper;


    }

    else
    {
    

    

        State=g_SecondFighter->GetFighterState();
  
/*        
        if(State==FS_STANCE||State==FS_CROUCH||State==FS_FALLING
            ||State==FS_LANDING||State==FS_PUNCH||State==FS_KICK
            ||State==FS_PUNCHC||State==FS_KICKC||State==FS_HADOKEN
            ||State==FS_JUMPU||State==FS_PUNCHU||State==FS_KICKU)
        {

            g_SecondFighter2->SetPosX(g_SecondFighter->GetPosX()-g_SecondFighter2->GetCollisionRect().left
            +g_SecondFighter->GetCollisionRect().left);
        
        }
        if(State==FS_WALKF)
        {
            g_SecondFighter2->SetPosX(g_SecondFighter->GetPosX()-g_SecondFighter2->GetCollisionRect().left
            +g_SecondFighter->GetCollisionRect().left-g_SecondFighter2->GetWalkVelocity());
        }
        if(State==FS_WALKB)
        {
           g_SecondFighter2->SetPosX(g_SecondFighter->GetPosX()-g_SecondFighter2->GetCollisionRect().left
           +g_SecondFighter->GetCollisionRect().left+g_SecondFighter2->GetWalkVelocity());
   
        }
        if(State==FS_JUMPUB)
        {
            g_SecondFighter2->SetPosX(g_SecondFighter->GetPosX()-g_SecondFighter2->GetCollisionRect().left
            +g_SecondFighter->GetCollisionRect().left+g_SecondFighter2->GetJumpVelocity().x);
        
        }
        if(State==FS_JUMPUF||State==FS_KICKUF||State==FS_PUNCHUF)
        {
             g_SecondFighter2->SetPosX(g_SecondFighter->GetPosX()-g_SecondFighter2->GetCollisionRect().left
            +g_SecondFighter->GetCollisionRect().left-g_SecondFighter2->GetJumpVelocity().x);
        }

*/

        g_SecondFighter2->SetPosX(g_SecondFighter->GetPosX()-g_SecondFighter2->GetCollisionRect().left
            +g_SecondFighter->GetCollisionRect().left);
        

        
        g_SecondFighter2->SetPosY(g_SecondFighter->GetPosY());

        g_SecondFighter2->SetGVelo(g_SecondFighter->GetGVelo());
        
        g_SecondFighter2->ForceState(g_SecondFighter->GetFighterState());

        g_SecondFighter2->SetPower(g_SecondFighter->GetPower());

        g_SecondFighter2->GetFire()->SetHidden(TRUE);//g_FirstFighter->GetFire()->IsHidden());

        //g_FirstFighter2->GetFire()->SetPos(g_FirstFighter->GetFire()->GetPosX(),g_FirstFighter->GetFire()->GetPosY());

        g_SecondFighter2->SetStateTime(g_SecondFighter->GetStateTime());

        g_SecondFighter=g_SecondFighter2;


        
        InputSwapper=CKEY_BASLEFT;
        CKEY_BASLEFT=CKEY_BASRIGHT;
        CKEY_BASRIGHT=InputSwapper;
    }

}
