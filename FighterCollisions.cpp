#include "FighterX.h"


extern CFighter *g_FirstFighter;
extern CFighter *g_FirstFighter1;
extern CFighter *g_FirstFighter2;
extern CDXSprite *g_FirstSplash;



extern CFighter *g_SecondFighter;
extern CFighter *g_SecondFighter1;
extern CFighter *g_SecondFighter2;
extern CDXSprite *g_SecondSplash;

extern CSprite *Explosion;




void CheckBlFire()
{
    
    //g_SecondFighter->Lock();
    //g_FirstFighter->GetFire()->Lock();

    if(!g_FirstFighter->GetFire()->IsHidden())
    {
        
        if((g_SecondFighter->SpriteHitPixel((CDXSprite*)(g_FirstFighter->GetFire())) )&&
            (g_SecondFighter->GetFighterState()!=FS_HIT))
        {
            g_SecondFighter->SetFighterState(FS_HIT);
            g_SecondFighter->SetPower(g_SecondFighter->GetPower()-10);
            return;
        }

    }


    
    //g_SecondFighter->UnLock();
    //g_FirstFighter->GetFire()->UnLock();


}

void CheckBasFire()
{

    //g_FirstFighter->Lock();
    //g_SecondFighter->GetFire()->Lock();


    if(!g_SecondFighter->GetFire()->IsHidden())
    {
  
        if((g_FirstFighter->SpriteHitPixel((CDXSprite*)(g_SecondFighter->GetFire()))  )&&
            (g_FirstFighter->GetFighterState()!=FS_HIT))
        {
            g_FirstFighter->SetFighterState(FS_HIT);
            g_FirstFighter->SetPower(g_FirstFighter->GetPower()-10);
            return;
        }

    }
    
    //g_FirstFighter->UnLock();
    //g_SecondFighter->GetFire()->UnLock();

}





void cdx_CheckSpriteCollisions()
{
    
    if(g_SecondFighter->SpriteHitPixel(g_FirstFighter))
    {
        FIGHTERSTATE FSTATE,SSTATE;
        FSTATE=g_FirstFighter->GetFighterState();
        SSTATE=g_SecondFighter->GetFighterState();
       
        if(FSTATE==SSTATE)
        {
            if(((g_SecondFighter->GetStateTime())<=(g_FirstFighter->GetStateTime())))
            {
                if(g_SecondFighter->SetHit(FSTATE)&&Explosion->IsHidden())
				{
					Explosion->SetHidden(FALSE);
					Explosion->SetPos(g_SecondFighter->GetPosX(),g_SecondFighter->GetPosY());
				}
            }
            else
            {
                if(g_FirstFighter->SetHit(SSTATE)&&Explosion->IsHidden())
				{
					Explosion->SetHidden(FALSE);
					Explosion->SetPos(g_FirstFighter->GetPosX(),g_FirstFighter->GetPosY());
				}
            }
            return;
        }
		else
		{

			if(g_FirstFighter->SetHit(SSTATE)&&Explosion->IsHidden())
			{
				Explosion->SetHidden(FALSE);
				Explosion->SetPos(g_FirstFighter->GetPosX()+g_FirstFighter->GetCollisionRect().left,g_FirstFighter->GetPosY());
			}
			if(g_SecondFighter->SetHit(FSTATE)&&Explosion->IsHidden())
			{
				Explosion->SetHidden(FALSE);
				Explosion->SetPos(g_SecondFighter->GetPosX()+g_SecondFighter->GetCollisionRect().right+g_SecondFighter->GetCollisionRect().left,g_SecondFighter->GetPosY());
			}
		}        
        
    }
    


    CheckBlFire();
 
    CheckBasFire();

}



